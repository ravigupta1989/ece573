#ifndef IRNODE_H
#define IRNODE_H

#include "symbol.h"
#include <string>
#include <cctype>
#include <sstream>

//using namespace std;

enum Operation
{
    ADD_OP, SUB_OP, MULT_OP, DIV_OP,LT_OP,GT_OP,E_OP,NE_OP,GE_OP,LE_OP,
};

class IRNode
{
    public:
    string opcode;
    Variable *firstOperand;
    Variable *secondOperand;
    int intLiteral;
    float floatLiteral;
    Variable *result;
    
    bool isIntLiteral;
    bool isFloatLiteral;
    
    IRNode(string code, Variable *op1, Variable *op2, Variable *r)
    {
        opcode = code;
        firstOperand = op1;
        secondOperand = op2;
        result = r;
        isIntLiteral = false;
        isFloatLiteral = false;
    }
    
    IRNode(string code, int iLiteral, Variable *op2, Variable *r)
    {
        
        opcode = code;
        intLiteral = iLiteral;        
        result = r;
        isIntLiteral = true;
        isFloatLiteral = false;
    }
    
    IRNode(string code, float fLiteral, Variable *op2, Variable *r)
    {
        
        opcode = code;
        floatLiteral = fLiteral;        
        result = r;
        isIntLiteral = false;
        isFloatLiteral = true;
    }
    
    IRNode(string code, Variable *r)
    {
        opcode = code;
        firstOperand = NULL;
        secondOperand = NULL;
        result = r;
        isIntLiteral = false;
        isFloatLiteral = false;
    }
	
    IRNode(string code)
    {
        opcode = code;
        firstOperand = NULL;
        secondOperand = NULL;
        result = NULL;
        isIntLiteral = false;
        isFloatLiteral = false;
    }
};

class tinyNode
{
    public:
    string opcode;
	string opr;
	string reg;

    tinyNode(string n_opcode, string n_pr, string n_reg)
    {
        opcode = n_opcode;
		opr=n_pr;
		reg=n_reg;
    }

    tinyNode(string n_opcode, string n_reg)
    {
        opcode = n_opcode;
		opr="";
		reg=n_reg;
    }
	tinyNode(string n_opcode)
    {
        opcode = n_opcode;
		opr="";
		reg="";
    }



};

class tinyNodeList
{
    public:
    vector<tinyNode*> tinyLines;
	
    int tempVarNum;
	int maxregs;
    tinyNodeList()
    {
        tempVarNum = 0;
		maxregs=0;
    }
    
    void addNode(tinyNode* node)
    {
        tinyLines.push_back(node);
		maxregs=0;
    }

    void print()
    {
		
        for (int i = 0; i < tinyLines.size(); i++)
        {
            
            cout<<tinyLines[i]->opcode<<" ";
			if (tinyLines[i]->opr != "")
         	   {cout<<tinyLines[i]->opr<<" ";}
         	cout<<tinyLines[i]->reg;

            cout<<endl;
        }

	    }

};


class IRNodeList
{
    public:
    vector<IRNode*> codeLines;
	string name;	
    int tempVarNum;
    string temp;
	string temp2;
	string temp3;
	string oprs,regs;
	std::stringstream buff;
    int LocalVars;
	int ParameterVars;
	int maxregs;
	//tinyNodeList* tnl2;
 
    IRNodeList(string nam)
    {
		name=nam;
        tempVarNum = 0;
		maxregs=0;
        LocalVars=0;
    }
    
    IRNodeList()
    {
        tempVarNum = 0;
		maxregs=0;
        LocalVars=0;
    }
    void addNode(IRNode* node)
    {
        codeLines.push_back(node);
    }

	string convert(string a)
		{
     
			string t;
			string tc;
			string ty;
			string stemp;
			int regnum;
            stringstream intString;
			
            tc=a.substr(0,1);
			ty=a.substr(1,1);
            
			if (tc =="$")
				if (ty=="T")
					{
					stemp = a.substr(2,(a.size()-2));
					regnum=atoi(stemp.c_str());
					if (regnum >= maxregs )
						{
							maxregs=regnum;	
						}
						return ("r" + a.substr(2,(a.size()-2)));}
				else if(ty=="L")
					{return ("$-" + a.substr(2,(a.size()-2)));}
				else if(ty=="P")
					{
                           
                        stemp = a.substr(2,(a.size()-2));
					    regnum=atoi(stemp.c_str());
                        intString.str("");
                        intString << regnum+2+maxregs;
						return ("$" + intString.str());
                    }
				else if(ty=="R")
					{
                           
                        stemp = a.substr(2,(a.size()-2));
					    regnum=atoi(stemp.c_str());
                        intString.str("");
                        intString << ParameterVars+2+maxregs;
						return ("$" + intString.str());
                    }

				else{return "NULL";}	
			else 
				return a;
			
		}
	//to check if memory val or not
	bool checkmem(string a)
		{
			if (a.substr(0,2) =="$T")
				{return false;}
			else {return true;}
		}	
 
    void print()
    {	
        for (int i = 0; i < codeLines.size(); i++)
        {
            cout<<";";
            cout<<codeLines[i]->opcode<<" ";
            if (codeLines[i]->isIntLiteral)
            {
                cout<<codeLines[i]->intLiteral<<" ";
            }
            else if (codeLines[i]->isFloatLiteral)
                cout<<codeLines[i]->floatLiteral<<" ";
            else
            {
                if (codeLines[i]->firstOperand != NULL)
                    cout<<codeLines[i]->firstOperand->mappedName<<" "; //mapped Name
                if (codeLines[i]->secondOperand != NULL)
                    cout<<codeLines[i]->secondOperand->mappedName<<" ";
            }
		 if (codeLines[i]->result != NULL)
			{ cout<<codeLines[i]->result->mappedName;}
            cout<<endl;
        }
		    }

	void gencode( tinyNodeList *tnl)
	{	
		maxregs=tnl->maxregs;
        stringstream intString;

		for (int i=0;i<codeLines.size(); i++)
		{
		  	
			temp2= codeLines[i]->opcode;
			if( (temp2 == "STOREI")|| (temp2 == "STOREF")) 
            {
                if( temp2 == "STOREI") {temp="move";} 
                if( temp2 == "STOREF") {temp="move";} 
                
				
              if ((codeLines[i]->isIntLiteral) || (codeLines[i]->isFloatLiteral))
			    {
               	 if (codeLines[i]->isIntLiteral)
               	 {
               	     buff<<codeLines[i]->intLiteral;
               	     oprs=buff.str();
               	     buff.str("");
               	 }
               	 else if (codeLines[i]->isFloatLiteral)
               	  {
					  buff<<codeLines[i]->floatLiteral;
               	      oprs=buff.str();
               	      buff.str("");
                  }
               	 else
               	 {
               	     if (codeLines[i]->firstOperand != NULL)
               	         {oprs=convert(codeLines[i]->firstOperand->mappedName);}
               	     if (codeLines[i]->secondOperand != NULL)
               	         {oprs=convert(codeLines[i]->secondOperand->mappedName);} 
               	 }
				
                	regs=convert(codeLines[i]->result->mappedName);        
                	tnl->addNode(new tinyNode(temp,oprs,regs));	
				}
			else {
					
					 if (codeLines[i]->firstOperand != NULL)
               	         {oprs=convert(codeLines[i]->firstOperand->mappedName);}
			
               	     else if (codeLines[i]->secondOperand != NULL)
               	         {oprs=convert(codeLines[i]->secondOperand->mappedName);} 

                	tnl->addNode(new tinyNode("move",oprs,"r0"));	
                	regs=convert(codeLines[i]->result->mappedName);        
                	tnl->addNode(new tinyNode(temp,"r0",regs));	

				  }
       
            }

			if( (temp2 == "EQ") || (temp2 == "NE") || (temp2 == "GE") || (temp2 == "LE") || (temp2 == "GT") || (temp2 == "LT") ) 
            {
                if( temp2 == "EQ") {temp="jeq";} 
                if( temp2 == "NE") {temp="jne";} 
                if( temp2 == "GE") {temp="jge";} 
                if( temp2 == "LE") {temp="jle";} 
                if( temp2 == "GT") {temp="jgt";} 
                if( temp2 == "LT") {temp="jlt";}
				if ( codeLines[i]->firstOperand->type == Int ) {temp3="cmpi";}
				if ( codeLines[i]->firstOperand->type == Float ) {temp3="cmpr";}
				oprs=convert(codeLines[i]->firstOperand->mappedName);	
                regs=convert(codeLines[i]->secondOperand->mappedName);
				if (checkmem(regs))
				  {
						tnl->addNode(new tinyNode("move",regs,"r0"));
						tnl->addNode(new tinyNode(temp3,oprs,"r0"));
				   }
				else
	             {   tnl->addNode(new tinyNode(temp3,oprs,regs));}		

                regs=convert(codeLines[i]->result->mappedName);
                tnl->addNode(new tinyNode(temp,regs));	 
                    
            }

	if( (temp2 == "LABEL") || (temp2 == "JUMP")|| (temp2 == "READI") || (temp2 == "WRITEI") || (temp2 == "READF") || (temp2 == "WRITEF") || (temp2 == "WRITES") || (temp2 == "READS") ) 
            {
                if( temp2 == "JUMP") {temp="jmp";} 
                if( temp2 == "LABEL") {temp="label";} 
                if( temp2 == "READI") {temp="sys readi";} 
                if( temp2 == "WRITEI") {temp="sys writei";} 
                if( temp2 == "READF") {temp="sys readr";} 
                if( temp2 == "WRITEF") {temp="sys writer";} 
                if( temp2 == "READS") {temp="sys reads";} 
                if( temp2 == "WRITES") {temp="sys writes";} 
                regs=convert(codeLines[i]->result->mappedName);        
                tnl->addNode(new tinyNode(temp,regs));	
                //tnl->print();      
            }

	if( (temp2 == "POP") || (temp2 == "PUSH") ) 
            {
                if( temp2 == "POP") {temp="pop";} 
                if( temp2 == "PUSH") {temp="push";}
				 
				if (codeLines[i]->result!=NULL) 
                	{ 
                      regs=convert(codeLines[i]->result->mappedName);        
                	  tnl->addNode(new tinyNode(temp,regs));
					}
				else {tnl->addNode(new tinyNode(temp)); }	      
            }

	if( temp2 == "LINK") 
            {
                if( temp2 == "LINK") {temp="link";} 
				    intString.str("");	
				    intString << (LocalVars-1);
    			    regs = intString.str(); 
                	tnl->addNode(new tinyNode(temp,regs));	      
            }
	    if(temp2 == "RET") 
            {
                if( temp2 == "RET") {temp="ret";} 
				
				//regs=convert(codeLines[i]->result->name);        
                	  tnl->addNode(new tinyNode("unlnk"));
					  tnl->addNode(new tinyNode(temp)); 
                 
                //tnl->print();      
            }
 if(temp2 == "JSR") 
            {
                for(int k=0; k<=maxregs;k++)
			      {
   				    intString.str("");	
			   	    intString << k;
    			    regs = "r" + intString.str(); 
				    tnl->addNode(new tinyNode("push",regs));
			      }

                if( temp2 == "JSR") {temp="jsr";} 
			    //cout<<"max regs"<<maxregs<<endl;	
				regs=convert(codeLines[i]->result->mappedName);        
                tnl->addNode(new tinyNode(temp,regs));	
                 
                for(int k=maxregs; k>=0;k--)
			      {
   				     intString.str("");	
			   	     intString << k;
    			     regs = "r" + intString.str(); 
				     tnl->addNode(new tinyNode("pop",regs));
			      }

                //tnl->print();      
            }

			if( temp2 == "ADDI" || temp2 =="MULTI" ||temp2=="DIVI" || temp2=="SUBI"|| temp2 == "ADDF" || temp2 =="MULTF" ||temp2=="DIVF" ||temp2=="SUBF" ) 
            {
                if (temp2=="ADDI") {temp="addi";}
                if (temp2=="MULTI"){temp="muli";}
                if (temp2=="DIVI"){temp="divi";}
                if (temp2=="SUBI"){temp="subi";}
                if (temp2=="ADDF") {temp="addr";}
                if (temp2=="MULTF"){temp="mulr";}
                if (temp2=="DIVF"){temp="divr";}
                if (temp2=="SUBF"){temp="subr";}

                oprs=convert(codeLines[i]->firstOperand->mappedName);	
                regs="r0";
                tnl->addNode(new tinyNode("move",oprs,regs));		

                if (codeLines[i]->secondOperand != NULL)
                    {oprs=convert(codeLines[i]->secondOperand->mappedName);}
                 
                regs="r0";
                tnl->addNode(new tinyNode(temp,oprs,regs));	
 
                oprs="r0";
                regs=convert(codeLines[i]->result->mappedName);

                tnl->addNode(new tinyNode("move",oprs,regs));
            }
		//tnl->print();
 
        }
		tnl->maxregs=maxregs;
    } 

	tinyNodeList* tinystartappender(tinyNodeList* tnl)
	{
		tinyNodeList* tnl2;
		tnl2=new tinyNodeList();
		stringstream intString;

		tnl2->addNode(new tinyNode("push"));

		for(int k=0; k<=tnl->maxregs;k++)
			{
   				intString.str("");	
				intString << k;
    			regs = "r" + intString.str(); 
				tnl2->addNode(new tinyNode("push",regs));
			 }
		tnl2->addNode(new tinyNode("jsr","main"));
		tnl2->addNode(new tinyNode("sys halt"));
		
		//add to original tiny code
		if (!tnl2->tinyLines.empty()) 
            {
     			tnl2->tinyLines.insert( tnl2->tinyLines.end(), tnl->tinyLines.begin(), tnl->tinyLines.end());
                tnl->tinyLines.clear();
				tnl->tinyLines.reserve(tnl2->tinyLines.size());
				copy(tnl2->tinyLines.begin(),tnl2->tinyLines.end(),back_inserter(tnl->tinyLines));	
				tnl2->tinyLines.clear();
            }	
		return tnl;	

	}



};

class IRTable
{
	public:
	vector <IRNodeList*> IRT; //global Table for IR code for each function 	

	void addIRTable(IRNodeList* irc)
	{
		IRT.push_back(irc);
	}

	void print()
	{
		cout<<";IR code"<<endl;
		for (int i = 0; i < IRT.size(); i++)
        {
            IRT[i]->print();
        }
		cout<<";tiny code"<<endl;

	}

	void gencode(tinyNodeList *tnl)
	{
	
		for (int i = 0; i < IRT.size(); i++)
        {	
            IRT[i]->gencode(tnl);
        }
        tnl->tinyLines.clear();
        for (int i = 0; i < IRT.size(); i++)
        {	
            IRT[i]->gencode(tnl);
        }

		IRT[0]->tinystartappender(tnl);

		//cout<<"maxregs"<<tnl->maxregs<<endl;

	}

};


#endif //IRNODE_H
