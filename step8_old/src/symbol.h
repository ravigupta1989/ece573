#ifndef SYMBOL_H
#define SYMBOL_H

#include <iostream>
#include <vector>
#include <string>
#include <map>
#include <sstream>

using namespace std;

enum Type
{
	Int, Float, String, Block
};


enum TypeLPR
{
	Local, Parameter, Result
};

class Variable
{
    public:    
    string name;
    Type type;
    string string_val;
	TypeLPR tlpr;   
	string mappedName;	
 
    Variable(string var_name, Type data_type)
    {
        name = var_name;
		mappedName = name;
        type = data_type;

    }
    
    Variable(string var_name, Type data_type, string value)
    {
        name = var_name;
		mappedName = name;
        type = data_type;
        string_val = value;
	
    }

    Variable(string var_name, Type data_type, string value, TypeLPR tlpr1)
    {
        name = var_name;
		mappedName = name;
        type = data_type;
        string_val = value;
		tlpr=tlpr1;
    }
	
 	Variable(Variable * var)  //copy Constructor  used in Liveness
    {
        name = var->name;
		mappedName=var->mappedName;
		// if (var->tlpr!=NULL)
			// {tlpr=var->tlpr;}
		type=var->type;	
    }

    
    void print()
    {
        cout<<name<<" "<<type<<endl;
    }
};

struct FunctionInfo
{
    string name;
    Type type;
    vector<Variable*> args;
};

class SymbolTable
{
	public:
	
	//data members
	string name;
    Type type;
	
    vector<Variable*> variables;
	SymbolTable *parentTable;
	vector<SymbolTable*> childTables;
	vector<string> conflicts;
	int Blocknum;
	int LocalVarNum;
	int ParameterVarNum;
    Type currType;
    
	string currentIdentifier;
	string currentStringVal;
	
	
	//std::map<Variable*, Variable*> VMAP;	//Map of Parameters and Local variables
	
	SymbolTable (string table_name, Type table_type)
	{
		name = table_name;
        type = table_type;
		Blocknum=0;
		LocalVarNum=1;
		ParameterVarNum=1;
	}	

	bool addVariable(Variable* d)
	{
		if (parentTable != NULL)
			checkConflict(d->name, parentTable);
		if (!checkDeclr(d->name))
			return false;
		variables.push_back(d);
		return true;
	}
	
	void addTable(SymbolTable* s)
	{
		childTables.push_back(s);
	}

	string createLocalParName(TypeLPR tlpr)
    {
       if (tlpr==Local)
         {
           stringstream intString;
           intString << LocalVarNum++;
           string s = "$L" + intString.str();
           return s;

         }
       else if (tlpr==Parameter)
         {
            
           stringstream intString;
           intString << ParameterVarNum++;
           string s = "$P" + intString.str();
           return s;

         }
        else {return NULL;}
    }

	void CreateVarMapName()
	{
        for (int ijk=0;ijk<variables.size();ijk++)
        {
            if (variables[ijk]->name == variables[ijk]->mappedName)
            {
                variables[ijk]->mappedName=createLocalParName(variables[ijk]->tlpr); 
            }
		   //cout<<variables[ijk]->name<<" "<<variables[ijk]->mappedName<<endl; 	
	    }
	 }
		
	void checkConflict(string decl_name, SymbolTable *table)
	{
		for (int i = 0; i < table->variables.size(); i++)
		{
			if (decl_name == table->variables[i]->name)
			{
				conflicts.push_back(decl_name);
				break;
			}
		}
		if (table->parentTable == NULL)
		{
			for (int i = 0; i < conflicts.size(); i++)
			{
				table->conflicts.push_back(conflicts[i]);
			}
			conflicts.clear();
		}
		else checkConflict(decl_name, table->parentTable);
	}
	
	bool checkDeclr(string decl_name)
	{
		for (int i = 0; i < variables.size(); i++)
		{
			if (decl_name == variables[i]->name)
			{
				cout<<"DECLARATION ERROR "<<decl_name<<endl;
				return false;
			}
		}
		return true;
	}
    
    Variable* findVariable(string decl_name)
    {
        for (int i = 0; i < variables.size(); i++)
        {
            if (decl_name == variables[i]->name)   //Ravi returning mapped variable
                return variables[i];
                //return (VMAP.find(variables[i]))->second;

        }
        //Getting here implies that the variable belongs to the parent. Search parent
        if (parentTable == NULL) //This should never happen
            return NULL;
        return parentTable->findVariable(decl_name);
    }
    
    FunctionInfo findFunction(string functionName)
    {
        for (int i = 0; i < childTables.size(); i++)
        {
            if (functionName == childTables[i]->name)
            {
                FunctionInfo functionInfo = {childTables[i]->name, childTables[i]->type};
                return functionInfo;
            }
        }
        //Getting here implies that the variable belongs to the parent. Search parent
        if (parentTable == NULL) //This should never happen
        {
            FunctionInfo functionInfo = {"", Int};
            return functionInfo;
        }
        return parentTable->findFunction(functionName); 
    }
	
	void print()
	{
		for (int i = 0; i < conflicts.size(); i++)
		{
			cout<<"SHADOW WARNING "<<conflicts[i]<<endl;
		}
		
		cout<<"Symbol table "<<name<<endl;
		for (int i = 0; i < variables.size(); i++)
		{
			cout<<"name "<<variables[i]->name;  //Mapped name
			switch (variables[i]->type)
			{
				case Int:
					cout<<" type INT";
					break;
				case Float:
					cout<<" type FLOAT";
					break;
				case String:
					cout<<" type STRING value "<<variables[i]->string_val;
					break;
				default:
					cout<<endl;
					break;
			}

            switch (variables[i]->tlpr)
			{
				case Local:
					cout<<" Local"<<endl;
					break;
				case Parameter:
					cout<<" Parameter"<<endl;
					break;
				case Result:
					cout<<" RESULT "<<endl;
					break;
				default:
					cout<<endl;
					break;
			}
			
		}
		cout<<endl;
		
		for (int i = 0; i < childTables.size(); i++)
		{
			childTables[i]->print();
		}
	}

    void printVar()
	{
		for (int i = 0; i < variables.size(); i++)
		{
            if (variables[i]->type == String)
                cout<<"str "<<variables[i]->name<<" "<<variables[i]->string_val<<endl;
			else cout<<"var "<<variables[i]->name<<endl;
        }
    }
};
#endif //SYMBOL_H
