#!/bin/bash
#Bash Script for executing all testcases

make clean
make all


fileArray=(factorial2 fibonacci2 fma fma_ravi step4_testcase step4_testcase2 step4_testcase3 test_573 test_adv test_expr test_for test_if)
for FILE in "${fileArray[@]}"
do
    printf "\n==========================================\n"
    printf $FILE
    printf "\n\n"
    ./Micro ./testcases/input/$FILE.micro > test.out
	java -jar step7.jar ./testcases/input/$FILE.micro > test1.out
    if [ $FILE == test_expr ] || [ $FILE == step4_testcase3 ]
    then
        ./tiny/tiny4 test.out
        printf "\n--------------------------------------\n\n"
        ./tiny/tiny4 test1.out
    else
        ./tiny/tiny4 test.out < testcases/input/$FILE.input
        printf "\n--------------------------------------\n\n"
        ./tiny/tiny4 test1.out < testcases/input/$FILE.input
    fi
done

