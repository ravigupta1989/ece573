#ifndef IRNODE_H
#define IRNODE_H

#include "symbol.h"
#include <string>
#include <cctype>
#include <sstream>

using namespace std;

enum Operation
{
    ADD_OP, SUB_OP, MULT_OP, DIV_OP,LT_OP,GT_OP,E_OP,NE_OP,GE_OP,LE_OP,
};

class RegMapping
{
    public:
    string regName;
    string varName;
    bool isFree;
    bool isDirty;
    
    RegMapping(int number)
    {
        stringstream intString;
        intString << number;
        regName = "r" + intString.str();
        varName = "";
        isFree = true;
        isDirty = false;
    }
};


class IGNode   //Interference Graph Node
{
	public:
	Variable *var;
	vector<IGNode*>edges ;

};

class IRNode
{
    public:
    string opcode;
    Variable *firstOperand;
    Variable *secondOperand;
    int intLiteral;
    float floatLiteral;
    Variable *result;
    
    bool isIntLiteral;
    bool isFloatLiteral;
	bool leader;	
   
	vector<int> succ;  //sucessor
	vector<int> pred;  //predecessor

	vector<Variable*> GEN;
	vector<Variable*> KILL;
	vector<Variable*> IN;
	vector<Variable*> OUT;
	 
    IRNode(string code, Variable *op1, Variable *op2, Variable *r)
    {
        opcode = code;
        firstOperand = op1;
        secondOperand = op2;
        result = r;
        isIntLiteral = false;
        isFloatLiteral = false;
		leader=false;
    }
    
    IRNode(string code, int iLiteral, Variable *op2, Variable *r)
    {
        
        opcode = code;
        intLiteral = iLiteral;        
        result = r;
        isIntLiteral = true;
        isFloatLiteral = false;
		leader=false;	
    }
    
    IRNode(string code, float fLiteral, Variable *op2, Variable *r)
    {
        
        opcode = code;
        floatLiteral = fLiteral;        
        result = r;
        isIntLiteral = false;
        isFloatLiteral = true;
		leader=false;
    }
    
    IRNode(string code, Variable *r)
    {
        opcode = code;
        firstOperand = NULL;
        secondOperand = NULL;
        result = r;
        isIntLiteral = false;
        isFloatLiteral = false;
		leader=false;
    }
	
    IRNode(string code)
    {
        opcode = code;
        firstOperand = NULL;
        secondOperand = NULL;
        result = NULL;
        isIntLiteral = false;
        isFloatLiteral = false;
		leader=false;
    }

	void addGEN(Variable* var)
    {
        GEN.push_back(var);
    }
	void addKILL(Variable* var)
    {
        KILL.push_back(var);
    }

	bool checkIN(Variable* var)
    {                    
        for (int hf=0;hf<IN.size();hf++)
            {
                if(IN[hf]->name == var->name)
                        {return true;} 		
            }	
        return false;
    }


	void removeIN()
    {
        for (int de=0;de<KILL.size();de++)
            {
            for (int hf=0;hf<IN.size();hf++)
                {
                    if(IN[hf]->name == KILL[de]->name)
                        {IN.erase(IN.begin()+hf);} 		
                }		
            }
    }
	void populateIN()
    {
        for (int hj=0;hj<OUT.size();hj++)
            {
                if (!checkIN(OUT[hj]))
                    { IN.push_back(OUT[hj]); }   //add GEN to IN 
            }
        
        for (int hj=0;hj<GEN.size();hj++)
            {
                if (!checkIN(GEN[hj]))
                    { IN.push_back(GEN[hj]); }   //add GEN to IN 
            }

        //removing KILL set from IN
        removeIN();	
    }


	void populateOUT(Variable* var)
    {
        OUT.push_back(var);
    }

};

class tinyNode
{
    public:
    string opcode;
	string opr;
	string reg;

    tinyNode(string n_opcode, string n_pr, string n_reg)
    {
        opcode = n_opcode;
		opr=n_pr;
		reg=n_reg;
    }

    tinyNode(string n_opcode, string n_reg)
    {
        opcode = n_opcode;
		opr="";
		reg=n_reg;
    }
	tinyNode(string n_opcode)
    {
        opcode = n_opcode;
		opr="";
		reg="";
    }

};

class tinyNodeList
{
    public:
    vector<tinyNode*> tinyLines;
	
    int tempVarNum;
    
    tinyNodeList()
    {
        tempVarNum = 0;
    }
    
    void addNode(tinyNode* node)
    {
        tinyLines.push_back(node);
    }

    void print()
    {		
        for (int i = 0; i < tinyLines.size(); i++)
        {
            
            cout<<tinyLines[i]->opcode<<" ";
            if (tinyLines[i]->opr != "")
         	   {cout<<tinyLines[i]->opr<<" ";}
         	cout<<tinyLines[i]->reg;
            cout<<endl;
        }
    }

};


class IRNodeList
{
    public:
    vector<IRNode*> codeLines;
	vector<IGNode*> igLines;
	string name;	
    int tempVarNum;
    string temp;
	string temp2;
	string temp3;
	string oprs,regs;
	std::stringstream buff;
    int LocalVars;
	int ParameterVars;
	int maxregs;
	//tinyNodeList* tnl2;
	int linkresult; 
    
    vector<RegMapping> regMap;
 
    IRNodeList(string nam, int numReg)
    {
		name=nam;
        tempVarNum = 0;
        maxregs = numReg;
        LocalVars=0;
        
        for (int i = 0; i < numReg; i++)
        {
            RegMapping rMap(i);
            regMap.push_back(rMap);
        }
    }
    
    IRNodeList(int numReg)
    {
        tempVarNum = 0;
        maxregs = numReg;
        LocalVars=0;
        for (int i = 0; i < numReg; i++)
        {
            RegMapping rMap(i);
            regMap.push_back(rMap);
        }
    }
    void addNode(IRNode* node)
    {
        codeLines.push_back(node);
    }

	string convert(string a)
    {
        string t;
        string tc;
        string ty;
        string stemp;
        int regnum;
        stringstream intString;
        
        tc=a.substr(0,1);
        ty=a.substr(1,1);
        
        if (tc =="$")
            if (ty=="T")
            {
                stemp = a.substr(2,(a.size()-2));
                regnum=atoi(stemp.c_str());
                return "allocate";
            }
            else if(ty=="L")
                {return ("$-" + a.substr(2,(a.size()-2)));}
            else if(ty=="P")
            {                   
                stemp = a.substr(2,(a.size()-2));
                regnum=atoi(stemp.c_str());
                intString.str("");
                intString << regnum+2+maxregs-1;
                return ("$" + intString.str());
            }
            else if(ty=="R")
            {                   
                stemp = a.substr(2,(a.size()-2));
                regnum=atoi(stemp.c_str());
                intString.str("");
                intString << ParameterVars+2+maxregs-1;
                return ("$" + intString.str());
            }
            else {return "NULL";}	
        else 
            return a;
        
    }
	
    //to check if memory val or not //OBSOLETE
	bool checkmem(string a)
    {
        if (a.substr(0,2) =="$T")
            {return false;}
        else {return true;}
    }
    
    
    int findRegister (string varName)
    {
        for (int i = 0; i < regMap.size(); i++)
        {
            if (regMap[i].varName == varName)
                return i;
        }
        return -1;
    }

    string allocateOperand(string varName, vector<Variable*> liveVars, tinyNodeList *tnl)
    {
        //Ensure var is in register are in registers
        string regName = ensure(varName, liveVars, tnl);
        
        //Free dead variables
        bool isDeadLater = true;
        for (int i = 0; i < liveVars.size(); i++)
        {
            if (varName == liveVars[i]->mappedName)
            {
                isDeadLater = false;
            }
        }
        if (isDeadLater)
        {
            int regNum = findRegister(varName);
            free(regMap[regNum].regName, liveVars, tnl);
        }
        
        return regName;    
    }
    
    string ensure(string varName, vector<Variable*> liveVars, tinyNodeList *tnl)
    {
        for (int i = 0; i < regMap.size(); i++)
        {
            if (regMap[i].varName == varName)
                return regMap[i].regName;
        }
        
        //Getting here implies that variable is not in register
        string regName = allocateResult(varName, "", liveVars, tnl);
        tnl->addNode(new tinyNode ("move", varName, regName));
        return regName;
    }

    void free(string regName, vector<Variable*> liveVars, tinyNodeList *tnl)
    {
        int regNum = atoi(regName.substr(1,1).c_str());
        if (regMap[regNum].isDirty)
        {
            for (int i = 0; i < liveVars.size(); i++)
            {
                if (regMap[regNum].varName == liveVars[i]->name)
                {
                    tnl->addNode(new tinyNode("move", regMap[regNum].regName, regMap[regNum].varName));
                    break;
                }
            }
        }
        regMap[regNum].isFree = true;
        regMap[regNum].isDirty = false;
    }

    string allocateResult(string varName, string currVarName, vector<Variable*> liveVars, tinyNodeList *tnl)
    {
		
        string regName = "";
        int regNum = -1;
        for (int i = 0; i < regMap.size(); i++)
        {
            if (regMap[i].isFree || regMap[i].varName == varName)
            {
                regName = regMap[i].regName;
                regNum = i;
                break;
            }
        }
        
        if (regName == "") //No free register
        {
            bool isCurrentVar = true;
            int freeIndex = -1;
            for (int i = 0; i < regMap.size(); i++)
            {
                
                if (regMap[i].varName != currVarName)
                {
                    isCurrentVar = false;
                    freeIndex = i;
                    break;
                }
            }
            if (!isCurrentVar) //We found a non-current register
            {
                regName = regMap[freeIndex].regName;
                regNum = freeIndex;
            }
            
        }
        if (regName == "") 
        {
            cout<<"Houston we have a problem. Could not assign register to "<<varName<<endl;
            cout<<"Current operand "<<varName<<endl;
            cout<<"Reg map is: "<<endl;
            for (int i = 0; i < regMap.size(); i++)
            {
                cout<<"Reg Name "<<regMap[i].regName<<"  ";
                cout<<"Var Name "<<regMap[i].varName<<endl;
            }
        }
        
        free(regName, liveVars, tnl);
        regMap[regNum].varName = varName;
        regMap[regNum].isFree = false;
        regMap[regNum].isDirty = false;
        return regName;
    }
 
    void print()
    {	
        for (int i = 0; i < codeLines.size(); i++)
        {
            // cout<<i<<";";
            cout<<";";
            cout<<codeLines[i]->opcode<<" ";
            if (codeLines[i]->isIntLiteral)
            {
                cout<<codeLines[i]->intLiteral<<" ";
            }
            else if (codeLines[i]->isFloatLiteral)
                cout<<codeLines[i]->floatLiteral<<" ";
            else
            {
                if (codeLines[i]->firstOperand != NULL)
                    cout<<codeLines[i]->firstOperand->mappedName<<" "; //mapped Name
                if (codeLines[i]->secondOperand != NULL)
                    cout<<codeLines[i]->secondOperand->mappedName<<" ";
            }
		 if (codeLines[i]->result != NULL)
			{ cout<<codeLines[i]->result->mappedName;}
            cout<<endl;
        }
	 }

	int sfind(IRNode* irn)
    {
        for (int i=0; i< codeLines.size(); i++)	
        {
            if((codeLines[i]->opcode == irn->opcode) && (codeLines[i]->result->name == irn->result->name))
            {
                return i;
            }

        }
        return 320000; //default return
    } 

	void populateCFG()
	 {
		string opc;
		IRNode * tempNode;
		int ks;
		//vector<IRNode*>::iterator titr;
		for (int i=0; i< codeLines.size(); i++)	
        {
            opc = codeLines[i]->opcode;
        
            if (opc == "JUMP")
            {                
                tempNode = new IRNode("LABEL" ,codeLines[i]->result ); 
                ks=sfind(tempNode);	
                if (ks!=320000)				
                {
                    //defining successor and predecessor for Jump statements
                    codeLines[i]->succ.push_back(ks);
                    codeLines[ks]->pred.push_back(i);
                }
                delete tempNode;	
            }
            else if ((opc == "GT" )|| (opc == "GE" )|| (opc == "LT")|| (opc == "LE")|| (opc == "NE")|| (opc == "EQ") )
            {
                tempNode = new IRNode("LABEL" ,codeLines[i]->result ); 
                ks=sfind(tempNode);	
                if (ks!=320000)				
                {
                    //defining successor and predecessor for Jump statements
                    codeLines[i]->succ.push_back(ks); // first target
                    codeLines[ks]->pred.push_back(i);
                    codeLines[i]->succ.push_back(i+1); // fall back path
                    codeLines[i+1]->pred.push_back(i);
                }
                delete tempNode;	
                //	cout<<"conditional found"<<endl;
            }
            else if (opc == "RET")
                {  }
            else 
            {	
                codeLines[i]->succ.push_back(i+1);
                codeLines[i+1]->pred.push_back(i);
            }
        }
    }	

    void printCFG()
    {
        for (int i = 0; i < codeLines.size(); i++)
        {
            cout<<i<<";\t";
            cout<<codeLines[i]->opcode<<" ";
            if (codeLines[i]->isIntLiteral)
            {
                cout<<codeLines[i]->intLiteral<<" ";
            }
            else if (codeLines[i]->isFloatLiteral)
                cout<<codeLines[i]->floatLiteral<<" ";
            else
            {
                if (codeLines[i]->firstOperand != NULL)
                    cout<<codeLines[i]->firstOperand->mappedName<<" "; //mapped Name
                if (codeLines[i]->secondOperand != NULL)
                    cout<<codeLines[i]->secondOperand->mappedName<<" ";
            }
            if (codeLines[i]->result != NULL)
                { cout<<codeLines[i]->result->mappedName;}
		
            if (!codeLines[i]->succ.empty())
            {
                for (int fg=0;fg<codeLines[i]->succ.size();fg++)
                {
                    cout<<"-->"<<codeLines[(codeLines[i]->succ[fg])]->opcode;
                }

            }
            if (!codeLines[i]->pred.empty())
			{ 
				for (int fg=0;fg<codeLines[i]->pred.size();fg++)
                {
                    cout<<"<--"<<codeLines[(codeLines[i]->pred[fg])]->opcode;
                }

			}

            if (!codeLines[i]->GEN.empty())
			{ 
				cout<<" *GEN{ ";
				for (int ls = 0; ls < codeLines[i]->GEN.size(); ls++)
                {
                    cout<<codeLines[i]->GEN[ls]->mappedName;
                }
				cout<<"} ";
			}

		
            if (!codeLines[i]->KILL.empty())
			{
				cout<<" *KILL{ ";
				for (int ls = 0; ls < codeLines[i]->KILL.size(); ls++)
                {
                    cout<<codeLines[i]->KILL[ls]->mappedName<<" ";
                }
				cout<<"} ";
			}			
            
            // cout<<"          *IN{ ";
            cout<<"\t\t\t*IN{ ";
            if (!codeLines[i]->IN.empty())
			{
                for (int ls = 0; ls < codeLines[i]->IN.size(); ls++)
                {
                    cout<<codeLines[i]->IN[ls]->mappedName<<" ";
                }
			}
            cout<<"} ";

			// cout<<" *OUT{ ";
            cout<<"\t*OUT{ ";
            if (!codeLines[i]->OUT.empty())
			{
				for (int ls = 0; ls < codeLines[i]->OUT.size(); ls++)
                {
                    cout<<codeLines[i]->OUT[ls]->mappedName<<" ";
                }
			}

			cout<<"} ";
	
		 if (codeLines[i]->leader)
			{
                cout<<"  LEADER";
			}

            cout<<endl;
	  
        }

	 }

	void MERGE2(IRNode &A, IRNode &B, IRNode &C) // function to merge two variable vectors
    {
        int bre=0;	
        //vector<Variable*> C;
        C.OUT=A.IN;
        //copy(A.begin(),A.end(),back_inserter(C));
        for (int yp=0;yp<B.IN.size();yp++)
        {
            for (int sa=0;sa<C.IN.size();sa++)
            {
                if (C.IN[sa]->name == B.IN[yp]->name)
                    { bre=1;break;}
            }
            if (bre == 0)
                {C.OUT.push_back(B.IN[yp]); bre=0; }
            else {bre=0;}
        }		
    }

	void MERGE1(IRNode &A, IRNode &B) // function to merge two variable vectors
    {
        int bre=0;	
        //vector<Variable*> C;
        B.OUT=A.IN;	
    }

	void createGENKILL()
	{
		string op1;
		int breaker=0;
        //	cout<<"CREATING GEN/KILL"<<endl;
		for (int i = 0; i < codeLines.size(); i++)
        { 
            //cout <<i<<codeLines[i]->opcode<<endl;
			op1 = codeLines[i]->opcode;	
			if (op1=="JSR" || op1=="LINK" ||op1=="LABEL" || op1=="JUMP" || op1=="RET") 
				{   }
		
			else if (op1=="PUSH" ||op1=="WRITEI" || op1=="WRITEF" || op1=="WRITES") 
            { 
                if (codeLines[i]->result != NULL)
                    {codeLines[i]->addGEN(new Variable(codeLines[i]->result));}
            }
			else if ( (op1=="GT")|| (op1=="GE")|| (op1=="LT")|| (op1=="LE")|| (op1=="NE")|| (op1=="EQ"))
			{
                if (codeLines[i]->isIntLiteral)
                {    }
                else if (codeLines[i]->isFloatLiteral)
                {	}
                else
                {
                   if (codeLines[i]->firstOperand != NULL)
                       {codeLines[i]->addGEN(new Variable(codeLines[i]->firstOperand));}
                   if (codeLines[i]->secondOperand != NULL)
                       {codeLines[i]->addGEN(new Variable(codeLines[i]->secondOperand));}
                }
			}
			else {
                if (codeLines[i]->isIntLiteral)
                {    }
                else if (codeLines[i]->isFloatLiteral)
                {	}
                else
                {
                    if (codeLines[i]->firstOperand != NULL)
                        {codeLines[i]->addGEN(new Variable(codeLines[i]->firstOperand));}
                    if (codeLines[i]->secondOperand != NULL)
                        {codeLines[i]->addGEN(new Variable(codeLines[i]->secondOperand));}
               }               
               if (codeLines[i]->result != NULL)
                    {codeLines[i]->addKILL(new Variable(codeLines[i]->result));}
            }			
		}
	}


/*
	void populateINOUT()
	{
        int index;
        int indexp;
        int breaker=0;
        vector<int> returnindexes; // to store all the return statements which are the starting points.
			
		for (int i=0;i<codeLines.size(); i++)
        {
            if (codeLines[i]->opcode == "RET")  //recording all the returns of the function
                {returnindexes.push_back(i);}
        }
		for (int dsf=0;dsf<returnindexes.size();dsf++)
        {
            index=returnindexes[dsf];
            indexp=index;
            codeLines[index]->IN.clear();
            breaker=0; 
            while ((breaker!=1) && (!codeLines[index]->pred.empty()))
            {
                if(codeLines[index]->pred.size()==1)
                {
                    cout<<"1 "<<index<<endl;
                    index = codeLines[index]->pred[0];
                    
                    if(codeLines[index]->succ.size()==1)
                    {
                        cout<<"2 "<<index<<endl;
                        codeLines[index]->OUT.reserve(codeLines[indexp]->IN.size());
                        copy(codeLines[indexp]->IN.begin(),codeLines[indexp]->IN.end(),back_inserter(codeLines[index]->OUT));

                        codeLines[index]->populateIN();
                    }
             
                    else if(codeLines[index]->succ.size() > 1)
                    {
                        cout<<"3 "<<index<<endl;
                        MERGE2(*codeLines[codeLines[index]->succ[0]],*codeLines[codeLines[index]->succ[1]], *codeLines[index]);
                        
                        codeLines[index]->populateIN();
                        //cout<<codeLines[index]->IN.size()<<endl;
                    }
                    else {cout<<"4"<<endl;}
                }
                
                else if (codeLines[index]->pred.size()>1 )
                {
                    //predecessors more than 2 - hence copy all the live variables to both streams	
                    MERGE1(*codeLines[index],*codeLines[codeLines[index]->pred[0]]);
                    MERGE1(*codeLines[index],*codeLines[codeLines[index]->pred[1]]);
                    codeLines[index]->populateIN();
                }
                indexp=index;
            }

        }
	
	}
*/
	void populateINOUT2(SymbolTable* sym)
	{
        int index;	
        for (int i=0;i<codeLines.size();i++)
        {
            if (codeLines[i]->opcode=="RET")
            {  
                codeLines[i]->OUT=sym->variables;
            }
        }
        for (int i=0;i<codeLines.size();i++)
         {
            if(codeLines[i]->pred.size()==1)
            {
                //	cout<<"1 "<<i<<endl;
                index = codeLines[i]->pred[0];  //index is pedecessor
                codeLines[i]->populateIN();	

                if(codeLines[index]->succ.size()==1)
                {                    
                    //cout<<"2 "<<i<<endl;
                    MERGE1(*codeLines[i],*codeLines[codeLines[i]->pred[0]]);
                }

                else if(codeLines[index]->succ.size() > 1)
                {
                    //cout<<"3 "<<i<<endl;
                    MERGE2(*codeLines[codeLines[index]->succ[0]],*codeLines[codeLines[index]->succ[1]], *codeLines[index]);	
                    codeLines[i]->populateIN();
                    //cout<<codeLines[index]->IN.size()<<endl;
                }
                else {cout<<"4"<<endl;}
            }
                    
            else if (codeLines[i]->pred.size()>1 )
            { 
                //predecessors more than 2 - hence copy all the live variables to both streams
                codeLines[i]->populateIN();
                MERGE1(*codeLines[i],*codeLines[codeLines[i]->pred[0]]);
                MERGE1(*codeLines[i],*codeLines[codeLines[i]->pred[1]]);

            }
            //indexp=index;
         }
    }	


	void createBB()
		{
			//populate leaders
			
			codeLines[0]->leader=true;
		 	for (int i=1;i<codeLines.size();i++)
 			  {
				if (codeLines[i]->pred.size()>1)
					{  
						codeLines[i]->leader=true;
					}
				else if (codeLines[i]->pred.size()==1)
					{
					 if (codeLines[codeLines[i]->pred[0]]->succ.size()>1)
						{
						codeLines[i]->leader=true;
						}
					}
			  }
	
		}

	void updateIG(vector<Variable*> varlist,vector<IGNode*> &IG)
		{
			vector<vector<Variable*> > powerset;
			vector<Variable*> tele;
		    cout<<"Variables:";	
			for (int ge=0;ge<varlist.size();ge++)
				{
					cout<<varlist[ge]->name<<" ";
				}
			cout<<endl;

			powerset.clear();
			
			for (int tr=0;tr<varlist.size()-1;tr++)
				{
					tele.clear();				
					for (int tk=tr+1;tk<varlist.size();tk++)
						{
								tele.push_back(varlist[tr]);
								tele.push_back(varlist[tk]);
								powerset.push_back(tele);
								tele.clear();	
						} 
				} 
			//--Powerset filled
			for (int ge=0;ge<powerset.size();ge++)
				{
					cout<<powerset[ge][0]->name<<" ";
					cout<<powerset[ge][1]->name<<" ";
				}
			cout<<endl;
		
		/*	for (int tr=0;tr<varlist.size();tr++)
				{  
					for (int wq=0;wq<IG.size();wq++)
						{ 
                			if(varlist)	
                		}

				}
		*/
		}

	void regalloc()
	{
		vector<int> tlm;  //BB charectoriser
	    //REGISTER ALLOCATION BY GRAPH COLORING 	
		//create INTERFERENCE GRAPH
		int i=0;
		while (i<codeLines.size())
			{
				if (codeLines[i]->leader==true)    //Start afresh becuase new Basic block started 
					{
			
						//clear IG
						igLines.clear();
					}
			
			//Update IG	
				for (int de=0;de<codeLines[i]->OUT.size();de++)
					{
						updateIG(codeLines[i]->OUT,igLines );	
					}
				i++;	
			}
	/*	
		for (;(codeLines[i+1]->leader==false);i++)
			{
		      cout<<i<<endl;	
				for (int de=0;de<codeLines[i]->OUT.size();de++)
					{
						updateIG(codeLines[i]->OUT,igLines );	
					}
				cout<<"_______"<<endl;
			}
	
	*/	
	}


	void gencode(SymbolTable* symtable, tinyNodeList *tnl)
	{
        stringstream intString;
        string currVar;
        string tempRegs;
		bool isDeadLater1;
		vector<Variable*> liveVars1;	 	

		for (int i=0;i<codeLines.size(); i++)
        {
            temp2= codeLines[i]->opcode;
			if( (temp2 == "STOREI")|| (temp2 == "STOREF")) 
            {
                temp = "move";
                				
                if ((codeLines[i]->isIntLiteral) || (codeLines[i]->isFloatLiteral))
			    {
                    if (codeLines[i]->isIntLiteral)
                    {
                        buff<<codeLines[i]->intLiteral;                        
                    }
                    else if (codeLines[i]->isFloatLiteral)
                    {
                        buff<<codeLines[i]->floatLiteral;                        
                    }
                    oprs=buff.str();
                    buff.str("");
                    
                    regs=convert(codeLines[i]->result->mappedName);
                    if (regs == "allocate")
                    {
                        regs = allocateResult(codeLines[i]->result->mappedName, currVar, codeLines[i]->OUT, tnl);
                    }
                    tnl->addNode(new tinyNode(temp,oprs,regs));
                }
                
                else 
                {
                    oprs=convert(codeLines[i]->firstOperand->mappedName);
                    if (oprs == "allocate")
                    {
                        currVar = codeLines[i]->firstOperand->mappedName;
                        oprs = allocateOperand(currVar, codeLines[i]->OUT, tnl);
                    }          

                    //Move operand into temp register. We should be able to optimise this away
                    tempRegs = allocateResult("tempR", currVar, codeLines[i]->OUT, tnl);
                    tnl->addNode(new tinyNode("move", oprs, tempRegs));
                    
                	regs=convert(codeLines[i]->result->mappedName);
                    if (regs == "allocate")
                    {
                        regs = allocateResult(codeLines[i]->result->mappedName, "temp", codeLines[i]->OUT, tnl);
                    }
                	
                    tnl->addNode(new tinyNode(temp, tempRegs, regs));
                    free(tempRegs, codeLines[i]->OUT, tnl);
                }
       
            }

			if( (temp2 == "EQ") || (temp2 == "NE") || (temp2 == "GE") || (temp2 == "LE") || (temp2 == "GT") || (temp2 == "LT") ) 
            {
                if( temp2 == "EQ") {temp="jeq";} 
                if( temp2 == "NE") {temp="jne";} 
                if( temp2 == "GE") {temp="jge";} 
                if( temp2 == "LE") {temp="jle";} 
                if( temp2 == "GT") {temp="jgt";} 
                if( temp2 == "LT") {temp="jlt";}
				if ( codeLines[i]->firstOperand->type == Int ) {temp3="cmpi";}
				if ( codeLines[i]->firstOperand->type == Float ) {temp3="cmpr";}
				
                currVar = codeLines[i]->firstOperand->mappedName;
                oprs=convert(codeLines[i]->firstOperand->mappedName);
                if (oprs == "allocate")
                {
                    oprs = allocateOperand(currVar, codeLines[i]->OUT, tnl);                   
                }
                
                regs=convert(codeLines[i]->secondOperand->mappedName);
                if (regs == "allocate")
                {
                    regs = allocateResult(codeLines[i]->secondOperand->mappedName, currVar, codeLines[i]->OUT, tnl);
                }
                else if (oprs == currVar && regs == codeLines[i]->secondOperand->mappedName) 
                {
                    //Both first and second operands are not register or stack values
                    //Move the second operand into register.
                    //Should probably include in ADDI group as well.
                    tempRegs = allocateResult("tempR", currVar, codeLines[i]->OUT, tnl);
                    tnl->addNode(new tinyNode("move", regs, tempRegs));
                    regs = tempRegs;
                }
				
                tnl->addNode(new tinyNode(temp3,oprs,regs));
                
                if (tempRegs == regs)
                    free(tempRegs, codeLines[i]->OUT, tnl);

                regs=convert(codeLines[i]->result->mappedName);
                if (regs == "allocate")
                {
                    regs = allocateResult(codeLines[i]->result->mappedName, "", codeLines[i]->OUT, tnl); //No current variables for this statement
                }
                tnl->addNode(new tinyNode(temp,regs));
            }

            if( (temp2 == "LABEL") || (temp2 == "JUMP")|| (temp2 == "READI") || (temp2 == "WRITEI") || (temp2 == "READF") || (temp2 == "WRITEF") || (temp2 == "WRITES") || (temp2 == "READS") ) 
            {
                if( temp2 == "JUMP") {temp="jmp";} 
                if( temp2 == "LABEL") {temp="label";} 
                if( temp2 == "READI") {temp="sys readi";} 
                if( temp2 == "WRITEI") {temp="sys writei";} 
                if( temp2 == "READF") {temp="sys readr";} 
                if( temp2 == "WRITEF") {temp="sys writer";} 
                if( temp2 == "READS") {temp="sys reads";} 
                if( temp2 == "WRITES") {temp="sys writes";} 
                
                regs=convert(codeLines[i]->result->mappedName); 
                if (regs == "allocate")
                {
                    regs = allocateResult(codeLines[i]->result->mappedName, "", codeLines[i]->OUT, tnl); //No current variables for this statement
                }
                tnl->addNode(new tinyNode(temp,regs));	
                // // tnl->print();  
            }

            if( (temp2 == "POP") || (temp2 == "PUSH") ) 
            {
                if( temp2 == "POP") {temp="pop";} 
                if( temp2 == "PUSH") {temp="push";}
				 
				if (codeLines[i]->result!=NULL) 
                {
                    regs=convert(codeLines[i]->result->mappedName);
                    if (regs == "allocate")
                    {
                        regs = allocateResult(codeLines[i]->result->mappedName, "", codeLines[i]->OUT, tnl); //No current variables for this statement
                    }
                    tnl->addNode(new tinyNode(temp,regs)); 
                }
				else {tnl->addNode(new tinyNode(temp)); }	      
            }

            if( temp2 == "LINK") 
            {
                if( temp2 == "LINK") {temp="link";} 
                intString.str("");	
                intString << (LocalVars-1);
                regs = intString.str(); 
                tnl->addNode(new tinyNode(temp,regs));	      
            }
            if(temp2 == "RET") 
            {
                if( temp2 == "RET") {temp="ret";}				
                tnl->addNode(new tinyNode("unlnk"));
                tnl->addNode(new tinyNode(temp)); 
                //tnl->print();      
            }
            if(temp2 == "JSR") 
            {
                for(int k=0; k < maxregs;k++)
                {
                    intString.str("");	
			   	    intString << k;
    			    regs = "r" + intString.str(); 
				    tnl->addNode(new tinyNode("push",regs));
                }
                
                //Free global variables
                for (int j = 0; j < symtable->variables.size(); j++)
                {
                    int regNum = findRegister(symtable->variables[j]->name);
                    if (regNum != -1)
                    {
                        free(regMap[regNum].regName, codeLines[i]->OUT, tnl);
                    }
                }
                
                temp = "jsr";
                // if( temp2 == "JSR") {temp="jsr";} 
			    
				regs=convert(codeLines[i]->result->mappedName);
                if (regs == "allocate")
                {
                    regs = allocateResult(codeLines[i]->result->mappedName, "", codeLines[i]->OUT, tnl); //No current variables for this statement
                }
                tnl->addNode(new tinyNode(temp,regs));
                 
                for(int k = maxregs - 1; k >= 0;k--)
                {
                    intString.str("");	
                    intString << k;
                    regs = "r" + intString.str(); 
                    tnl->addNode(new tinyNode("pop",regs));
                }
                //tnl->print();      
            }

			if( temp2 == "ADDI" || temp2 =="MULTI" ||temp2=="DIVI" || temp2=="SUBI"|| temp2 == "ADDF" || temp2 =="MULTF" ||temp2=="DIVF" ||temp2=="SUBF" ) 
            {
                if (temp2=="ADDI") {temp="addi";}
                if (temp2=="MULTI"){temp="muli";}
                if (temp2=="DIVI"){temp="divi";}
                if (temp2=="SUBI"){temp="subi";}
                if (temp2=="ADDF") {temp="addr";}
                if (temp2=="MULTF"){temp="mulr";}
                if (temp2=="DIVF"){temp="divr";}
                if (temp2=="SUBF"){temp="subr";}
                
                currVar = codeLines[i]->firstOperand->mappedName;
                oprs=convert(codeLines[i]->firstOperand->mappedName);
                if (oprs == "allocate")
                {
                    oprs = allocateOperand(codeLines[i]->firstOperand->mappedName, codeLines[i]->OUT, tnl);
                }
                
                //Move operand into a temp register. Optimisable
                // RAVI - If the variable not in register list then non need of temp variable(its freed)
               	isDeadLater1 = true;
				for (int ihjk = 0; ihjk < regMap.size(); ihjk++)
        			{
            			if (regMap[ihjk].varName == currVar)
                			{ isDeadLater1 = false;break; }
        			}
	
        		if (!isDeadLater1)
        			{  //Dont create Temporary vAriable - Go ahead with result variable first and use that register itself
        		      //  cout<<"not DeadLater"<<endl;	
                		//create Temporary Variable
                		tempRegs = allocateResult("tempR", currVar, codeLines[i]->OUT, tnl);
                		tnl->addNode(new tinyNode("move", oprs, tempRegs));
                
                		if (codeLines[i]->secondOperand != NULL)
                		{
                    		currVar = codeLines[i]->secondOperand->mappedName;
                    		oprs=convert(codeLines[i]->secondOperand->mappedName);
                    		if (oprs == "allocate")
                   			 {
                        		oprs = allocateOperand(codeLines[i]->secondOperand->mappedName, codeLines[i]->OUT, tnl);
                    		 }                    
                		}
                 
                		tnl->addNode(new tinyNode(temp,oprs,tempRegs));  //This line is Operation node
                
                		regs=convert(codeLines[i]->result->mappedName);
                		if (regs == "allocate")
                		{ 
                    		regs = allocateResult(codeLines[i]->result->mappedName, "temp", codeLines[i]->OUT, tnl);
                		}

               			 tnl->addNode(new tinyNode("move",tempRegs,regs));
                		 free(tempRegs, codeLines[i]->OUT, tnl);

					}
				else 
					{
						//cout<<"dead later"<<endl;
						//create Temporary Variable
                		tempRegs = allocateResult("tempR", currVar, codeLines[i]->OUT, tnl);
                		tnl->addNode(new tinyNode("move", oprs, tempRegs));
                
                		if (codeLines[i]->secondOperand != NULL)
                		{
                    		currVar = codeLines[i]->secondOperand->mappedName;
                    		oprs=convert(codeLines[i]->secondOperand->mappedName);
                    		if (oprs == "allocate")
                   			 {
                        		oprs = allocateOperand(codeLines[i]->secondOperand->mappedName, codeLines[i]->OUT, tnl);
                    		 }                    
                		}
                 
                		tnl->addNode(new tinyNode(temp,oprs,tempRegs));  //This line is Operation node
                
                		//regs=convert(codeLines[i]->result->mappedName);
						//update result Variable with Temp reg
						
						for (int ihjk = 0; ihjk < regMap.size(); ihjk++)
        					{
					//			cout<<regMap[ihjk].varName<<endl;
            					if (regMap[ihjk].varName == "tempR")
                					{ regMap[ihjk].varName = codeLines[i]->result->mappedName; break;}
        					}
                	//	free(tempRegs, codeLines[i]->OUT, tnl);
				}
            }
            //tnl->print();
        }
    } 

	tinyNodeList* tinystartappender(tinyNodeList* tnl)
	{
		tinyNodeList* tnl2;
		tnl2=new tinyNodeList();
		stringstream intString;

		tnl2->addNode(new tinyNode("push"));
        
        for(int k = 0; k < maxregs; k++)
        {
            intString.str("");	
            intString << k;
            regs = "r" + intString.str(); 
            tnl2->addNode(new tinyNode("push",regs));
         }
		tnl2->addNode(new tinyNode("jsr","main"));
		tnl2->addNode(new tinyNode("sys halt"));
		
		//add to original tiny code
		if (!tnl2->tinyLines.empty()) 
        {
            tnl2->tinyLines.insert( tnl2->tinyLines.end(), tnl->tinyLines.begin(), tnl->tinyLines.end());
            tnl->tinyLines.clear();
            tnl->tinyLines.reserve(tnl2->tinyLines.size());
            copy(tnl2->tinyLines.begin(),tnl2->tinyLines.end(),back_inserter(tnl->tinyLines));	
            tnl2->tinyLines.clear();
        }	
		return tnl;
	}

};

class IRTable
{
	public:
	vector <IRNodeList*> IRT; //global Table for IR code for each function 	
	
	void addIRTable(IRNodeList* irc)
	{
		IRT.push_back(irc);
	}

	void print()
	{
		cout<<";IR code"<<endl;
		for (int i = 0; i < IRT.size(); i++)
        {
            IRT[i]->print();
        }
		cout<<";tiny code"<<endl;

	}

	void gencode(SymbolTable* symtable, tinyNodeList *tnl)
	{
	
		for (int i = 0; i < IRT.size(); i++)
        {	
            IRT[i]->gencode(symtable, tnl);
        }
        // tnl->print();
        
        //No need to run a second time with fixed registers.
        // tnl->tinyLines.clear();
        // for (int i = 0; i < IRT.size(); i++)
        // {	
            // IRT[i]->gencode(tnl);
        // }

		IRT[0]->tinystartappender(tnl);
	}
    
    void populateCFG()
	{
	 	for (int i = 0; i< IRT.size();i++)
		{
			IRT[i]->populateCFG();	
		}
			
	 	for (int i = 0; i< IRT.size();i++)
		{
			IRT[i]->createBB();	
		}

	}
    
    void printCFG()
	{
	 	for (int i = 0; i< IRT.size();i++)
		{
			IRT[i]->printCFG();	
		}		

	}

    void Liveanalyse(SymbolTable* sym)
	{
	 	for (int i = 0; i< IRT.size();i++)
		{
			IRT[i]->createGENKILL();	
		}		
	 	
		for (int fg=0; fg<=10;fg++)	
        {
            for (int i = 0; i< IRT.size();i++)
            {
                IRT[i]->populateINOUT2(sym);	
            }
        }
		
		// for (int i = 0; i< IRT.size();i++)
        // {
            // cout<<"*******"<<endl;
            // IRT[i]->regalloc();	
        // }
	}

};


#endif //IRNODE_H
