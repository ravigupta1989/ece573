#!/bin/bash
#Bash Script for executing all testcases

make clean
make all
cd build
for FILE in {1..22}
do
	./Micro ../testcases/input/test$FILE.micro > testout.out
	diff -b -B testout.out ../testcases/output/test$FILE.out 
	echo "=============test$FILE executed============" 
done

