/*An example of using the flex C++ scanner class.*/

%{
#include "../src/symbol.h"
#include "../src/irnode.h"
#include "../generated/gram.tab.h"
//# undef yywrap
//# define yywrap() 1
%}

%option noyywrap

%%

[ \t\n\r]+   


PROGRAM         {return TPROGRAM;}
BEGIN		    {return T_BEGIN;}
END             {return TEND;}
FUNCTION		{return FUNCTION;}
READ			{return READ;}
WRITE			{return WRITE;}
IF				{return IF;}
ELSE			{return ELSE;}
FI				{return FI;}
FOR				{return FOR;}
ROF				{return ROF;}
CONTINUE		{return CONTINUE;}
BREAK     		{return BREAK;}
RETURN			{return RETURN;}
INT				{return INT;}
VOID			{return VOID;}
STRING			{return STRING;}
FLOAT         	{return FLOAT;}

":="			{return ASSIGN;}
"+"				{return PLUS;}
"-"				{return MINUS;}
"*"				{return MULT;}
"/"				{return DIV;}
"="				{return EQUAL;}
"!="			{return NEQUAL;}
"<"     		{return LESSTHAN;}
">"				{return GREATERTHAN;}
"("				{return OPENPAR;}
")"				{return CLOSEPAR;}
";"				{return SEMI;} 
","             {return COMMA;}
"<="			{return LEQUAL;}
">="			{return GEQUAL;}

[A-Za-z][0-9A-Za-z]*	{yylval.str_val = yytext;return IDENTIFIER;}

\"[^\n^\"]*\"			{yylval.str_val = yytext;return STRINGLITERAL;}

[0-9]*"."[0-9]+			{yylval.float_val=atof(yytext);return FLOATLITERAL;}

[0-9]+					{yylval.int_val=atoi(yytext); return INTLITERAL;}

"--".*\n				{}

.|\n

%%

