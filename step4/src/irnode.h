#ifndef IRNODE_H
#define IRNODE_H

#include "symbol.h"
#include <string>
#include <cctype>
using namespace std;

enum Operation
{
    ADD_OP, SUB_OP, MULT_OP, DIV_OP,
};

class Variable
{
    public:    
    string name;
    Type type;
    
    Variable(string var_name, Type data_type)
    {
        name = var_name;
        type = data_type;
    }
    
    void print()
    {
        cout<<name<<" "<<type<<endl;
    }
};

class IRNode
{
    public:
    string opcode;
    Variable *firstOperand;
    Variable *secondOperand;
    int intLiteral;
    float floatLiteral;
    Variable *result;
    
    bool isIntLiteral;
    bool isFloatLiteral;
    
    IRNode(string code, Variable *op1, Variable *op2, Variable *r)
    {
        opcode = code;
        firstOperand = op1;
        secondOperand = op2;
        result = r;
        isIntLiteral = false;
        isFloatLiteral = false;
    }
    
    IRNode(string code, int iLiteral, Variable *op2, Variable *r)
    {
        
        opcode = code;
        intLiteral = iLiteral;        
        result = r;
        isIntLiteral = true;
        isFloatLiteral = false;
    }
    
    IRNode(string code, float fLiteral, Variable *op2, Variable *r)
    {
        
        opcode = code;
        floatLiteral = fLiteral;        
        result = r;
        isIntLiteral = false;
        isFloatLiteral = true;
    }
    
    IRNode(string code, Variable *r)
    {
        opcode = code;
        firstOperand = NULL;
        secondOperand = NULL;
        result = r;
        isIntLiteral = false;
        isFloatLiteral = false;
    }
};

class IRNodeList
{
    public:
    vector<IRNode*> codeLines;
	
    int tempVarNum;
    string temp;
	string temp2; 
    IRNodeList()
    {
        tempVarNum = 0;
    }
    
    void addNode(IRNode* node)
    {
        codeLines.push_back(node);
    }

	string convert(string a)
		{
			string t;
			string tc;
			tc=a.substr(0,1);
			if (tc =="$")
				return ("r" + a.substr(2,(a.size()-2)));
			else 
				return a;
			
		}
	//to check if memory val or not
	bool checkmem(string a)
		{
			if (a.substr(0,1) =="$")
				{return false;}
			else {return true;}
		}	
 
    void print()
    {
		cout<<";IR code"<<endl;
		
        for (int i = 0; i < codeLines.size(); i++)
        {
            cout<<";";
            cout<<codeLines[i]->opcode<<" ";
            if (codeLines[i]->isIntLiteral)
            {
                cout<<codeLines[i]->intLiteral<<" ";
            }
            else if (codeLines[i]->isFloatLiteral)
                cout<<codeLines[i]->floatLiteral<<" ";
            else
            {
                if (codeLines[i]->firstOperand != NULL)
                    cout<<codeLines[i]->firstOperand->name<<" ";
                if (codeLines[i]->secondOperand != NULL)
                    cout<<codeLines[i]->secondOperand->name<<" ";
            }
            cout<<codeLines[i]->result->name;
            cout<<endl;
        }
		cout<<";tiny code"<<endl;
    }

	void printcode()
	{
		//cout<<";tiny code"<<endl;
		for (int i=0;i<codeLines.size(); i++)
		{
			temp="STOREI";
			temp2= codeLines[i]->opcode;
			if( (temp2 == "STOREI") || (temp2 == "WRITEI") || (temp2 == "STOREF") || (temp2 == "WRITEF") ) 
				{
					if( temp2 == "STOREI") {temp="move";} 
					if( temp2 == "WRITEI") {temp="sys writei";} 
					if( temp2 == "STOREF") {temp="move";} 
					if( temp2 == "WRITEF") {temp="sys writer";} 

					cout<<temp<<" ";
					if (codeLines[i]->isIntLiteral)
           			 {
                		cout<<codeLines[i]->intLiteral<<" ";
            		 }
            		else if (codeLines[i]->isFloatLiteral)
                		{cout<<codeLines[i]->floatLiteral<<" ";}
            		else
            		 {
                		if (codeLines[i]->firstOperand != NULL)
                    	cout<<convert(codeLines[i]->firstOperand->name)<<" ";
                		if (codeLines[i]->secondOperand != NULL)
                    	cout<<convert(codeLines[i]->secondOperand->name)<<" ";
            		 }
            			cout<<convert(codeLines[i]->result->name);
            			cout<<endl;	
				}
			if( temp2 == "ADDI" || temp2 =="MULTI" ||temp2=="DIVI" || temp2=="SUBI"|| temp2 == "ADDF" || temp2 =="MULTF" ||temp2=="DIVF" ||temp2=="SUBF" ) 
				{
					if (temp2=="ADDI") {temp="addi";}
					if (temp2=="MULTI"){temp="muli";}
					if (temp2=="DIVI"){temp="divi";}
					if (temp2=="SUBI"){temp="subi";}
					if (temp2=="ADDF") {temp="addr";}
					if (temp2=="MULTF"){temp="mulr";}
					if (temp2=="DIVF"){temp="divr";}
					if (temp2=="SUBF"){temp="subr";}
					cout<<"move"<<" ";
					cout<<convert(codeLines[i]->firstOperand->name)<<" ";	
					cout <<"r0"<<endl;
					
					cout<<temp<<" ";
					
                	if (codeLines[i]->secondOperand != NULL)
                    	{cout<<convert(codeLines[i]->secondOperand->name)<<" ";}
					 
  					cout<<"r0";
					cout<<endl;	
					cout << "move"<<" ";
	 
					
					cout<<"r0"<<" ";
            		cout<<convert(codeLines[i]->result->name);
					cout <<endl;

				} }
cout <<"sys halt"<<endl;

  } 
};
#endif //IRNODE_H
