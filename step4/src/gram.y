%{
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sstream>
#include "../src/symbol.h"
#include "../src/irnode.h"

SymbolTable *symtable; /*  The symbol table of the program*/
SymbolTable *currTable; //The current table in the parser
bool is_declaration = true;

Variable *IR_firstOperand;
Variable *IR_secondOperand;
Variable *IR_tempVariable;
Variable *IR_assignResult;
vector<string> IR_opcodes;
string IR_opcode;

IRNodeList *IRcode;

vector<string>idList;

extern FILE *yyin;
#define YYDEBUG 1

int yyparse(void);
extern int yylex(void);
 
int yyerror(const char *str)
{
	// fprintf(stderr,'error: %s\n',str);
	return 1;
}
 
int yywrap()
{
	return 3;
}

Declaration* addDeclaration(string name)
{
	Declaration *new_decl = new Declaration(name, currTable->currType);
	if (currTable->addDeclaration(new_decl))
		return new_decl;
	return NULL;
}

void addSymbolTable(string name)
{
	SymbolTable *newTable = new SymbolTable(name);
	newTable->parentTable = currTable;
	currTable->addTable(newTable);
	currTable = newTable;
}

void createNewBlock()
{
	symtable->Blocknum++;
	std::string s;
	std::stringstream out;
	out << symtable->Blocknum;
	s = "BLOCK " + out.str();
	addSymbolTable(s);
}

Variable* createTempVar(Type type)
{
    stringstream intString;
    intString << ++IRcode->tempVarNum;
    string s = "$T" + intString.str();
    Variable *tempVar = new Variable (s, type);
    return tempVar;
}

%}

%union{
	char* str_val;
	int int_val;
	float float_val;
	char char_val;
	
	Declaration* declaration;
    Operation operation;
    IRNode* irnode;
    Variable* variable;
}

//Bison token declarations
%token <str_val> IDENTIFIER;
%token <int_val> INTLITERAL;
%token <float_val> FLOATLITERAL;
%token <str_val>  STRINGLITERAL;
%token TPROGRAM T_BEGIN TEND FUNCTION READ WRITE IF ELSE FI FOR ROF CONTINUE BREAK RETURN INT VOID STRING FLOAT
%token ASSIGN PLUS MINUS MULT DIV EQUAL NEQUAL LESSTHAN GREATERTHAN OPENPAR CLOSEPAR SEMI COMMA LEQUAL GEQUAL
//%token IDENTIFIER INTLITERAL FLOATLITERAL STRINGLITERAL

%type <str_val> id str;
%type <declaration> string_decl;
%type <operation> addop mulop;
%type <variable> assign_expr expr expr_prefix factor factor_prefix postfix_expr primary;

%%
/*Grammar rules*/

/* Program */
program			: TPROGRAM id T_BEGIN pgm_body TEND
id				: IDENTIFIER
					{
                        $$ = $1;
						if (symtable == NULL) //The global symbol table hasn't been made yet
						{
							symtable = new SymbolTable("GLOBAL");
							symtable->parentTable = NULL;
							currTable = symtable;
                            
                            //Create code list
                            IRcode = new IRNodeList();
						}
						else
						{
                            currTable->currentIdentifier = $1;
						}
					}
pgm_body		: decl func_declarations
decl			: string_decl decl | var_decl decl | empty	

/* Global String Declaration */
string_decl 	: STRING id ASSIGN str SEMI
					{
						if (is_declaration)
						{
							Declaration *new_decl = addDeclaration(currTable->currentIdentifier);
							if (new_decl == NULL)
								YYABORT;
							else new_decl->createString(currTable->currentStringVal);
						}
						  
					}

str           	: STRINGLITERAL { currTable->currentStringVal = $1; currTable->currType = String; }

/* Variable Declaration */
var_decl		: var_type id_list SEMI
var_type		: FLOAT { currTable->currType = Float; }
                | INT { currTable->currType = Int; }
any_type		: var_type | VOID 
id_list			: id 
					{ 
						if (is_declaration && addDeclaration($1) == NULL) 
							YYABORT;
                        idList.clear(); //Clear idList before pushing new ids
                        idList.push_back($1);
					}
                  id_tail
id_tail      	: COMMA id
					{ 
						if (is_declaration && addDeclaration($2) == NULL) 
							YYABORT;
                        idList.push_back($2); //Add to global idList
					}
                  id_tail | empty

/* Function Paramater List */
param_decl_list		: param_decl param_decl_tail | empty
param_decl 			: var_type id 
						{ 
							if (is_declaration && addDeclaration($2) == NULL) 
								YYABORT;
						}
param_decl_tail		: COMMA param_decl param_decl_tail | empty

/* Function Declarations */
func_declarations	: func_decl func_declarations | empty
func_decl        	: FUNCTION any_type id { addSymbolTable(currTable->currentIdentifier); }
					  OPENPAR param_decl_list CLOSEPAR T_BEGIN func_body TEND
func_body         	: decl stmt_list 

/* Statement List  */
stmt_list         	: stmt stmt_list | empty
stmt             	: base_stmt | if_stmt | for_stmt
base_stmt         	: assign_stmt | read_stmt | write_stmt | return_stmt

/* Basic Statements */
assign_stmt       	: assign_expr SEMI
assign_expr       	: id
                        {
                            //Find the identifier in the symbol table
                            Declaration *identifier = currTable->findDeclaration($1);
                            if (identifier != NULL)
                            {
                                IR_assignResult = new Variable(identifier->name, identifier->data_type);
                            }
                            else cout<<"Non string identifier "<<$1<<" being initialised and declared!"<<endl;
                            
                        }
                      ASSIGN expr
                        {
                            IR_firstOperand = $4;
                            if (IR_assignResult->type == Int)
                                IRcode->addNode(new IRNode("STOREI", IR_firstOperand, NULL, IR_assignResult));
                            else
                                IRcode->addNode(new IRNode("STOREF", IR_firstOperand, NULL, IR_assignResult));
                        }
read_stmt         	: READ { is_declaration = false; }
					  OPENPAR id_list 
                        {
                            //Go through the idList and generate read instructions for each one
                            for (int i = 0; i < idList.size(); i++)
                            {
                                Declaration *identifier = currTable->findDeclaration(idList[i]);
                                IR_tempVariable = new Variable(identifier->name, identifier->data_type);
                                if (IR_tempVariable->type == Int)
                                    IRcode->addNode(new IRNode("READI", IR_tempVariable));
                                if (IR_tempVariable->type == Float)
                                    IRcode->addNode(new IRNode("READF", IR_tempVariable));
                            }
                        }
                      CLOSEPAR SEMI { is_declaration = true; }
write_stmt        	: WRITE { is_declaration = false; }
					  OPENPAR id_list 
                        {
                            //Go through the idList and generate write instructions for each one
                            for (int i = 0; i < idList.size(); i++)
                            {
                                Declaration *identifier = currTable->findDeclaration(idList[i]);
                                IR_tempVariable = new Variable(identifier->name, identifier->data_type);
                                if (IR_tempVariable->type == Int)
                                    IRcode->addNode(new IRNode("WRITEI", IR_tempVariable));
                                if (IR_tempVariable->type == Float)
                                    IRcode->addNode(new IRNode("WRITEF", IR_tempVariable));
                            }
                        }
                      CLOSEPAR SEMI { is_declaration = true; }
return_stmt       	: RETURN expr SEMI

/* Expressions */
expr              	: expr_prefix factor
                        {
                            if ($1 == NULL) //No previous operation, just a variable. Return it
                            {
                                $$ = $2;
                            }
                            else
                            {
                                IR_firstOperand = $1;
                                IR_secondOperand = $2;
                                IR_opcode = IR_opcodes.back();
                                
                                //Generate temporary variable to hold result
                                IR_tempVariable = createTempVar(IR_firstOperand->type);
                                
                                //Add code node to code list
                                IRcode->addNode(new IRNode(IR_opcode, IR_firstOperand, IR_secondOperand, IR_tempVariable));
                                
                                //Return tempVariable and remove opcode
                                $$ = IR_tempVariable;
                                IR_opcodes.pop_back();
                            }
                        }
expr_prefix       	: expr_prefix factor addop
                        {
                            if ($1 == NULL)
                            {
                                $$ = $2;
                                IR_firstOperand = $2;
                            }
                            else
                            {
                                IR_firstOperand = $1;
                                IR_secondOperand = $2;
                                IR_opcode = IR_opcodes.back();
                                
                                //Generate temporary variable to hold result
                                IR_tempVariable = createTempVar(IR_firstOperand->type);
                                
                                //Add code node to code list
                                IRcode->addNode(new IRNode(IR_opcode, IR_firstOperand, IR_secondOperand, IR_tempVariable));
                                
                                //Return tempVariable and remove opcode
                                $$ = IR_tempVariable;
                                IR_opcodes.pop_back();
                            }
                            
                            //Push opcode to stack
                            if ($3 == ADD_OP && IR_firstOperand->type == Int)
                                IR_opcodes.push_back("ADDI");
                            if ($3 == ADD_OP && IR_firstOperand->type == Float)
                                IR_opcodes.push_back("ADDF");
                            if ($3 == SUB_OP && IR_firstOperand->type == Int)
                                IR_opcodes.push_back("SUBI");
                            if ($3 == SUB_OP && IR_firstOperand->type == Float)
                                IR_opcodes.push_back("SUBF");
                            
                        }
                    | empty {$$ = NULL;}
factor            	: factor_prefix postfix_expr
                        {
                            if ($1 == NULL) //No operation, just a variable. Return it
                            {
                                $$ = $2;
                            }
                            else
                            {
                                IR_firstOperand = $1;
                                IR_secondOperand = $2;
                                IR_opcode = IR_opcodes.back();
                                
                                //Generate temporary variable to hold result
                                IR_tempVariable = createTempVar(IR_firstOperand->type);
                                
                                //Add code node to code list
                                IRcode->addNode(new IRNode(IR_opcode, IR_firstOperand, IR_secondOperand, IR_tempVariable));
                                
                                //Return tempVariable and remove opcode
                                $$ = IR_tempVariable;
                                IR_opcodes.pop_back();
                            }
                        }
factor_prefix     	: factor_prefix postfix_expr mulop
                        {
                            if ($1 != NULL)
                            {
                                IR_firstOperand = $1;
                                IR_secondOperand = $2;
                                IR_opcode = IR_opcodes.back();
                                
                                //Generate temporary variable to hold result
                                IR_tempVariable = createTempVar(IR_firstOperand->type);
                                
                                //Add code node to code list
                                IRcode->addNode(new IRNode(IR_opcode, IR_firstOperand, IR_secondOperand, IR_tempVariable));
                                
                                //Return tempVariable and remove opcode
                                $$ = IR_tempVariable;
                                IR_opcodes.pop_back();
                            }
                            else
                            {
                                //Factor prefix is empty. Return $2
                                $$ = $2;
                                IR_firstOperand = $2;
                            }
                            
                            //Push opcode to stack
                            if ($3 == MULT_OP && IR_firstOperand->type == Int)
                                 IR_opcodes.push_back("MULTI");
                            if ($3 == MULT_OP && IR_firstOperand->type == Float)
                                IR_opcodes.push_back("MULTF");
                            if ($3 == DIV_OP && IR_firstOperand->type == Int)
                                IR_opcodes.push_back("DIVI");
                            if ($3 == DIV_OP && IR_firstOperand->type == Float)
                                IR_opcodes.push_back("DIVF");
                        }
                    | empty {$$ = NULL;}
postfix_expr      	: primary {$$ = $1;}
                    | call_expr {$$ = NULL;} //Temporary
call_expr         	: id OPENPAR expr_list CLOSEPAR
expr_list         	: expr expr_list_tail | empty
expr_list_tail    	: COMMA expr expr_list_tail | empty
primary           	: OPENPAR expr CLOSEPAR { $$ = $2; }
                    | id 
                        {
                            //Find the variable in the symbol table and return it as primary
                            //We use currTable->currentIdentifier because for some reason $1 does not return the actual id.
                            Declaration *identifier = currTable->findDeclaration(currTable->currentIdentifier);
                            if (identifier != NULL)
                                $$ = new Variable(identifier->name, identifier->data_type);
                            else cout<<"Non string identifier "<<$1<<" being initialised and declared!"<<endl;
                        }
                    | INTLITERAL
                        {
                            //When we see a literal we must generate a temporary variable
                            IR_tempVariable = createTempVar(Int);
                            //Generate code to store INTLITERAL ($1) in temp variable
                            IRcode->addNode(new IRNode("STOREI", $1, NULL, IR_tempVariable));
                            //Return the temp variable as primary
                            $$ = IR_tempVariable;
                        }
                    | FLOATLITERAL
                        {
                            //When we see a literal we must generate a temporary variable
                            IR_tempVariable = createTempVar(Float);
                            //Generate code to store FLOATLITERAL ($1) in temp variable
                            IRcode->addNode(new IRNode("STOREF", $1, NULL, IR_tempVariable));
                            //Return the temp variable as primary
                            $$ = IR_tempVariable;
                        }
addop             	: PLUS {$$ = ADD_OP;} | MINUS {$$ = SUB_OP;}
mulop             	: MULT {$$ = MULT_OP;} | DIV {$$ = DIV_OP;}

/* Complex Statements and Condition */ 
if_stmt           	: IF OPENPAR cond { createNewBlock(); }
					  CLOSEPAR decl stmt_list { currTable=currTable->parentTable; }
					  else_part FI
else_part         	: ELSE { createNewBlock(); }
					  decl stmt_list { currTable=currTable->parentTable; } | empty
						
cond              	: expr compop expr
compop            	: LESSTHAN | GREATERTHAN | EQUAL | NEQUAL | GEQUAL | LEQUAL

init_stmt         	: assign_expr | empty
incr_stmt         	: assign_expr | empty

/* ECE 573 students use this version of for_stmt */
for_stmt       		: FOR OPENPAR init_stmt SEMI cond SEMI incr_stmt CLOSEPAR {	createNewBlock(); }
					  decl aug_stmt_list ROF { currTable=currTable->parentTable; } 

/* CONTINUE and BREAK statements. ECE 573 students only */
aug_stmt_list     	: aug_stmt aug_stmt_list | empty
aug_stmt          	: base_stmt | aug_if_stmt | for_stmt | CONTINUE SEMI | BREAK SEMI

/* Augmented IF statements for ECE 573 students */ 
aug_if_stmt       	: IF OPENPAR cond { createNewBlock(); }
					  CLOSEPAR decl aug_stmt_list { currTable=currTable->parentTable; }
					  aug_else_part FI
aug_else_part     	: ELSE { createNewBlock(); }
					  decl aug_stmt_list { currTable=currTable->parentTable; } | empty

empty				:

%%

//Epilogue
int main(int argc, char **argv)
 {
	// yydebug=1;
	++argv, --argc;
	if(argc > 0)
	{
		yyin = fopen( argv[0], "r");
	}
	else
	{
		yyin = stdin;
	}

	int c = 0;
	c = yyparse();
	if(c == 0)
	{
        IRcode->print();
		symtable->printcode();
		IRcode->printcode();
	}
	return 0;
 }
