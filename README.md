# README #
This project is a compiler framework built using flex/bison in C/C++. It is an OOP program which converts LITTLE program into TINY binary. The specs, grammar and other details are not available to me in raw format but is not difficult to reverse engineer from file in the code. Some of the optimizations implemented in this project are - Register Allocation, CSE, Instruction selection and available expression I am releasing this project as an open source as a help to all people out there who could use this framework in their compilers project or learn from it. I am not contributing to it anymore actively. 

Please note that the makefile is included in all steps and step 8 is the final code with all details. 

Ravi Gupta - Designer and Implementer

#Acknowledgements#
I co-designed it as my semester wide project. Special thanks to my Project Partner John Ojogbo and Dr. Milind Kulkarni (my compilers Professor) without whom completing this Project would have been a nightmare.