#ifndef SYMBOL_H
#define SYMBOL_H

#include <iostream>
#include <vector>
#include <string>
using namespace std;

enum Type
{
	Int, Float, String
};

class Declaration
{
	public:
	
	//data members
	string name;
	Type data_type;
	bool has_value;
	long long int int_val;
	float float_val;
	string string_val;

	//member functions	
	Declaration(string decl_name, Type type)
	{
		name = decl_name;
		data_type = type;
		has_value = false;
	}

	void createInt(long long int_va)
	{
		int_val = int_va;
		has_value = true;
	}
	
	void createFloat(float float_va)
	{
		float_val = float_va;
		has_value = true;
	}

	void createString(string str_val)
	{
		string_val = str_val;
		has_value = true;
	}
};

class SymbolTable
{
	public:
	
	//data members
	string name;
	vector<Declaration*> declarations;
	SymbolTable *parentTable;
	vector<SymbolTable*> childTables;
	Declaration* currDeclaration;
	string currentIdentifier;
	int currentIntVal;
	float currentFloatVal;
	string currentStringVal;
	Type currType;
	int Blocknum;
	vector<string> conflicts;
	
	//member functions
	SymbolTable (string table_name)
	{
		name = table_name;
		Blocknum=0;
	}	

	bool addDeclaration(Declaration* d)
	{
		currDeclaration = d;
		if (parentTable != NULL)
			checkConflict(d->name, parentTable);
		if (!checkDeclr(d->name))
			return false;
		declarations.push_back(d);
		return true;
	}
	
	void addTable(SymbolTable* s)
	{
		childTables.push_back(s);
	}
	
	void checkConflict(string decl_name, SymbolTable *table)
	{
		for (int i = 0; i < table->declarations.size(); i++)
		{
			if (decl_name == table->declarations[i]->name)
			{
				conflicts.push_back(decl_name);
				break;
			}
		}
		if (table->parentTable == NULL)
		{
			for (int i = 0; i < conflicts.size(); i++)
			{
				table->conflicts.push_back(conflicts[i]);
			}
			conflicts.clear();
		}
		else checkConflict(decl_name, table->parentTable);
	}
	
	bool checkDeclr(string decl_name)
	{
		for (int i = 0; i < declarations.size(); i++)
		{
			if (decl_name == declarations[i]->name)
			{
				cout<<"DECLARATION ERROR "<<decl_name<<endl;
				return false;
			}
		}
		return true;
	}
	
	void print()
	{
		for (int i = 0; i < conflicts.size(); i++)
		{
			cout<<"SHADOW WARNING "<<conflicts[i]<<endl;
		}
		
		cout<<"Symbol table "<<name<<endl;
		for (int i = 0; i < declarations.size(); i++)
		{
			cout<<"name "<<declarations[i]->name;
			switch (declarations[i]->data_type)
			{
				case Int:
					cout<<" type INT";
					if (declarations[i]->has_value)
						cout<<" value "<<declarations[i]->int_val;
					cout<<endl;
					break;
				case Float:
					cout<<" type FLOAT";
					if (declarations[i]->has_value)
						cout<<" value "<<declarations[i]->float_val;
					cout<<endl;
					break;
				case String:
					cout<<" type STRING";
					if (declarations[i]->has_value)
						cout<<" value "<<declarations[i]->string_val;
					cout<<endl;
					break;
				default:
					cout<<endl;
					break;
			}
			
		}
		cout<<endl;
		
		for (int i = 0; i < childTables.size(); i++)
		{
			childTables[i]->print();
		}
	}
};
#endif //SYMBOL_H
