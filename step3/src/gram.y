%{
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sstream>
#include "../src/symbol.h"
SymbolTable *symtable; /*  The symbol table of the program*/
SymbolTable *currTable; //The current table in the parser
bool is_declaration = true;

extern FILE *yyin;
#define YYDEBUG 1

int yyparse(void);
extern int yylex(void);
 
int yyerror(const char *str)
{
	// fprintf(stderr,'error: %s\n',str);
	return 1;
}
 
int yywrap()
{
	return 3;
}

Declaration* addDeclaration(string name)
{
	Declaration *new_decl = new Declaration(name, currTable->currType);
	if (currTable->addDeclaration(new_decl))
		return new_decl;
	return NULL;
}

void addSymbolTable(string name)
{
	SymbolTable *newTable = new SymbolTable(name);
	newTable->parentTable = currTable;
	currTable->addTable(newTable);
	currTable = newTable;
}

void createNewBlock()
{
	symtable->Blocknum++;
	std::string s;
	std::stringstream out;
	out << symtable->Blocknum;
	s = "BLOCK " + out.str();
	addSymbolTable(s);
}


%}

%union{
	char* str_val;
	int int_val;
	float float_val;
	char char_val;
	
	Declaration *declaration;    
}

//Bison token declarations
%token <str_val> IDENTIFIER;
%token <int_val> INTLITERAL;
%token <float_val> FLOATLITERAL;
%token <str_val>  STRINGLITERAL;
%token TPROGRAM T_BEGIN TEND FUNCTION READ WRITE IF ELSE FI FOR ROF CONTINUE BREAK RETURN INT VOID STRING FLOAT
%token ASSIGN PLUS MINUS MULT DIV EQUAL NEQUAL LESSTHAN GREATERTHAN OPENPAR CLOSEPAR SEMI COMMA LEQUAL GEQUAL  
//%token IDENTIFIER INTLITERAL FLOATLITERAL STRINGLITERAL

%type <str_val> id str; 
%type <declaration> string_decl;

%%
/*Grammar rules*/

/* Program */
program			: TPROGRAM id T_BEGIN pgm_body TEND
id				: IDENTIFIER
					{
						if (symtable == NULL) //The global symbol table hasn't been made yet
						{
							symtable = new SymbolTable("GLOBAL");
							symtable->parentTable = NULL;
							currTable = symtable;
						}
						else
						{
							$$ = $1;
							//cout<<"identifier is "<<$1<<endl;
							currTable->currentIdentifier = $1;
						}
					}
pgm_body		: decl func_declarations
decl			: string_decl decl | var_decl decl | empty	

/* Global String Declaration */
string_decl 	: STRING id ASSIGN str SEMI
					{
						if (is_declaration)
						{
							Declaration *new_decl = addDeclaration(currTable->currentIdentifier);
							if (new_decl == NULL)
								YYABORT;
							else new_decl->createString(currTable->currentStringVal);
						}
						  
					}

str           	: STRINGLITERAL { currTable->currentStringVal = $1; currTable->currType = String; }

/* Variable Declaration */
var_decl		: var_type id_list SEMI
var_type		: FLOAT { currTable->currType = Float; }
                | INT { currTable->currType = Int; }
any_type		: var_type | VOID 
id_list			: id 
					{ 
						if (is_declaration && addDeclaration($1) == NULL) 
							YYABORT;
					}
                  id_tail
id_tail      	: COMMA id
					{ 
						if (is_declaration && addDeclaration($2) == NULL) 
							YYABORT;
					}
                  id_tail | empty

/* Function Paramater List */
param_decl_list		: param_decl param_decl_tail | empty
param_decl 			: var_type id 
						{ 
							if (is_declaration && addDeclaration($2) == NULL) 
								YYABORT;
						}
param_decl_tail		: COMMA param_decl param_decl_tail | empty

/* Function Declarations */
func_declarations	: func_decl func_declarations | empty
func_decl        	: FUNCTION any_type id { addSymbolTable(currTable->currentIdentifier); }
					  OPENPAR param_decl_list CLOSEPAR T_BEGIN func_body TEND
func_body         	: decl stmt_list 

/* Statement List  */
stmt_list         	: stmt stmt_list | empty
stmt             	: base_stmt | if_stmt | for_stmt
base_stmt         	: assign_stmt | read_stmt | write_stmt | return_stmt

/* Basic Statements */
assign_stmt       	: assign_expr SEMI
assign_expr       	: id ASSIGN expr
read_stmt         	: READ { is_declaration = false; }
					  OPENPAR id_list CLOSEPAR SEMI { is_declaration = true; }
write_stmt        	: WRITE { is_declaration = false; }
					  OPENPAR id_list CLOSEPAR SEMI { is_declaration = true; }
return_stmt       	: RETURN expr SEMI

/* Expressions */
expr              	: expr_prefix factor
expr_prefix       	: expr_prefix factor addop | empty
factor            	: factor_prefix postfix_expr
factor_prefix     	: factor_prefix postfix_expr mulop | empty
postfix_expr      	: primary | call_expr
call_expr         	: id OPENPAR expr_list CLOSEPAR
expr_list         	: expr expr_list_tail | empty
expr_list_tail    	: COMMA expr expr_list_tail | empty
primary           	: OPENPAR expr CLOSEPAR | id | INTLITERAL | FLOATLITERAL
addop             	: PLUS | MINUS
mulop             	: MULT | DIV

/* Complex Statements and Condition */ 
if_stmt           	: IF OPENPAR cond { createNewBlock(); }
					  CLOSEPAR decl stmt_list { currTable=currTable->parentTable; }
					  else_part FI
else_part         	: ELSE { createNewBlock(); }
					  decl stmt_list { currTable=currTable->parentTable; } | empty
						
cond              	: expr compop expr
compop            	: LESSTHAN | GREATERTHAN | EQUAL | NEQUAL | GEQUAL | LEQUAL

init_stmt         	: assign_expr | empty
incr_stmt         	: assign_expr | empty

/* ECE 573 students use this version of for_stmt */
for_stmt       		: FOR OPENPAR init_stmt SEMI cond SEMI incr_stmt CLOSEPAR {	createNewBlock(); }
					  decl aug_stmt_list ROF { currTable=currTable->parentTable; } 

/* CONTINUE and BREAK statements. ECE 573 students only */
aug_stmt_list     	: aug_stmt aug_stmt_list | empty
aug_stmt          	: base_stmt | aug_if_stmt | for_stmt | CONTINUE SEMI | BREAK SEMI

/* Augmented IF statements for ECE 573 students */ 
aug_if_stmt       	: IF OPENPAR cond { createNewBlock(); }
					  CLOSEPAR decl aug_stmt_list { currTable=currTable->parentTable; }
					  aug_else_part FI
aug_else_part     	: ELSE { createNewBlock(); }
					  decl aug_stmt_list { currTable=currTable->parentTable; } | empty

empty				:

%%

//Epilogue
int main(int argc, char **argv)
 {
	// yydebug=1;
	++argv, --argc;
	if(argc > 0)
	{
		yyin = fopen( argv[0], "r");
	}
	else
	{
		yyin = stdin;
	}

	int c = 0;
	c = yyparse();
	if(c == 0)
	{
		symtable->print();
	}
	return 0;
 }
