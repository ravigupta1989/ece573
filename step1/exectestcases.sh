#!/bin/bash
#Bash Script for executing all testcases

make clean
make compiler
cd build
for FILE in fibonacci nested loop loopbreak sqrt
do
	./Micro ../inputs/$FILE.micro > $FILE.out
	diff -b -B $FILE.out ../outputs/$FILE.out 
	echo "=============$FILE executed============" 
done

