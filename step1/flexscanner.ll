 // An example of using the flex C++ scanner class.

%{
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
using namespace std;
%}

%option noyywrap

alpha             [A-Za-z]
num               [0-9]
alphanum          [A-Za-z0-9]
identifier        ({alpha}{alphanum}*) 
stringliteral     \"[^\n^\"]*\"
floatliteral      {num}*\.{num}+
comment           \-\-[^\n]*\n

%%

[ \t\n\r]+          /* eat up whitespace*/

{comment}         /*eat up comments */

PROGRAM|BEGIN|END|FUNCTION|READ|WRITE cout<<"Token Type: KEYWORD\nValue: "<<YYText()<<'\n';

IF|ELSE|FI|FOR|ROF|CONTINUE|BREAK     cout<<"Token Type: KEYWORD\nValue: "<<YYText()<<'\n';

RETURN|INT|VOID|STRING|FLOAT          cout<<"Token Type: KEYWORD\nValue: "<<YYText()<<'\n';

":="|"+"|"-"|"*"|"/"|"="|"!="|"<"     cout<<"Token Type: OPERATOR\nValue: "<<YYText()<<'\n';

">"|"("|")"|";"|","|"<="|">="         cout<<"Token Type: OPERATOR\nValue: "<<YYText()<<'\n';

{identifier}                          cout<<"Token Type: IDENTIFIER\nValue: "<<YYText()<<'\n';

{stringliteral}                       cout<<"Token Type: STRINGLITERAL\nValue: "<<YYText()<<'\n';

{floatliteral}                        cout<<"Token Type: FLOATLITERAL\nValue: "<<YYText()<<'\n';

{num}+                                cout<<"Token Type: INTLITERAL\nValue: "<<YYText()<<'\n';

%%

int main( int argc , char* argv[])
{
    ifstream InputFile;
    ofstream OutputFile;
    InputFile.open(argv[1]);
    FlexLexer* lexer = new yyFlexLexer(&InputFile, &OutputFile);
    while(lexer->yylex() != 0)
        ;
    return 0;return 0;



}
 
