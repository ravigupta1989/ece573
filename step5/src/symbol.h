#ifndef SYMBOL_H
#define SYMBOL_H

#include <iostream>
#include <vector>
#include <string>
using namespace std;

enum Type
{
	Int, Float, String
};


class Variable
{
    public:    
    string name;
    Type type;
    string string_val;
    
    Variable(string var_name, Type data_type)
    {
        name = var_name;
        type = data_type;
    }
    
    Variable(string var_name, Type data_type, string value)
    {
        name = var_name;
        type = data_type;
        string_val = value;
    }
    
    void print()
    {
        cout<<name<<" "<<type<<endl;
    }
};

class SymbolTable
{
	public:
	
	//data members
	string name;
	vector<Variable*> variables;
	SymbolTable *parentTable;
	vector<SymbolTable*> childTables;
	Variable* currVariable;
	string currentIdentifier;
	string currentStringVal;
	Type currType;
	int Blocknum;
	vector<string> conflicts;
	
	//member functions
	SymbolTable (string table_name)
	{
		name = table_name;
		Blocknum=0;
	}	

	bool addVariable(Variable* d)
	{
		currVariable = d;
		if (parentTable != NULL)
			checkConflict(d->name, parentTable);
		if (!checkDeclr(d->name))
			return false;
		variables.push_back(d);
		return true;
	}
	
	void addTable(SymbolTable* s)
	{
		childTables.push_back(s);
	}
	
	void checkConflict(string decl_name, SymbolTable *table)
	{
		for (int i = 0; i < table->variables.size(); i++)
		{
			if (decl_name == table->variables[i]->name)
			{
				conflicts.push_back(decl_name);
				break;
			}
		}
		if (table->parentTable == NULL)
		{
			for (int i = 0; i < conflicts.size(); i++)
			{
				table->conflicts.push_back(conflicts[i]);
			}
			conflicts.clear();
		}
		else checkConflict(decl_name, table->parentTable);
	}
	
	bool checkDeclr(string decl_name)
	{
		for (int i = 0; i < variables.size(); i++)
		{
			if (decl_name == variables[i]->name)
			{
				cout<<"DECLARATION ERROR "<<decl_name<<endl;
				return false;
			}
		}
		return true;
	}
    
    Variable* findVariable(string decl_name)
    {
        for (int i = 0; i < variables.size(); i++)
        {
            if (decl_name == variables[i]->name)
                return variables[i];
        }
        //Getting here implies that the variable belongs to the parent. Search parent
        if (parentTable == NULL) //This should never happen
            return NULL;
        return parentTable->findVariable(decl_name);
    }
	
	void print()
	{
		for (int i = 0; i < conflicts.size(); i++)
		{
			cout<<"SHADOW WARNING "<<conflicts[i]<<endl;
		}
		
		cout<<"Symbol table "<<name<<endl;
		for (int i = 0; i < variables.size(); i++)
		{
			cout<<"name "<<variables[i]->name;
			switch (variables[i]->type)
			{
				case Int:
					cout<<" type INT"<<endl;
					break;
				case Float:
					cout<<" type FLOAT"<<endl;
					break;
				case String:
					cout<<" type STRING value "<<variables[i]->string_val<<endl;
					break;
				default:
					cout<<endl;
					break;
			}
			
		}
		cout<<endl;
		
		for (int i = 0; i < childTables.size(); i++)
		{
			childTables[i]->print();
		}
	}

    void printVar()
	{
		for (int i = 0; i < variables.size(); i++)
		{
            if (variables[i]->type == String)
                cout<<"str "<<variables[i]->name<<" "<<variables[i]->string_val<<endl;
			else cout<<"var "<<variables[i]->name<<endl;
        }
    }
};
#endif //SYMBOL_H
