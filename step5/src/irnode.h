#ifndef IRNODE_H
#define IRNODE_H

#include "symbol.h"
#include <string>
#include <cctype>
#include <sstream>
using namespace std;

enum Operation
{
    ADD_OP, SUB_OP, MULT_OP, DIV_OP,LT_OP,GT_OP,E_OP,NE_OP,GE_OP,LE_OP,
};

class IRNode
{
    public:
    string opcode;
    Variable *firstOperand;
    Variable *secondOperand;
    int intLiteral;
    float floatLiteral;
    Variable *result;
    
    bool isIntLiteral;
    bool isFloatLiteral;
    
    IRNode(string code, Variable *op1, Variable *op2, Variable *r)
    {
        opcode = code;
        firstOperand = op1;
        secondOperand = op2;
        result = r;
        isIntLiteral = false;
        isFloatLiteral = false;
    }
    
    IRNode(string code, int iLiteral, Variable *op2, Variable *r)
    {
        
        opcode = code;
        intLiteral = iLiteral;        
        result = r;
        isIntLiteral = true;
        isFloatLiteral = false;
    }
    
    IRNode(string code, float fLiteral, Variable *op2, Variable *r)
    {
        
        opcode = code;
        floatLiteral = fLiteral;        
        result = r;
        isIntLiteral = false;
        isFloatLiteral = true;
    }
    
    IRNode(string code, Variable *r)
    {
        opcode = code;
        firstOperand = NULL;
        secondOperand = NULL;
        result = r;
        isIntLiteral = false;
        isFloatLiteral = false;
    }
};

class tinyNode
{
    public:
    string opcode;
	string opr;
	string reg;

    tinyNode(string n_opcode, string n_pr, string n_reg)
    {
        opcode = n_opcode;
		opr=n_pr;
		reg=n_reg;
    }


    tinyNode(string n_opcode, string n_reg)
    {
        opcode = n_opcode;
		opr="";
		reg=n_reg;
    }

};

class tinyNodeList
{
    public:
    vector<tinyNode*> tinyLines;
	
    int tempVarNum;
    tinyNodeList()
    {
        tempVarNum = 0;
    }
    
    void addNode(tinyNode* node)
    {
        tinyLines.push_back(node);
    }

    void print()
    {
		
        for (int i = 0; i < tinyLines.size(); i++)
        {
            
            cout<<tinyLines[i]->opcode<<" ";
			if (tinyLines[i]->opr != "")
         	   {cout<<tinyLines[i]->opr<<" ";}
         	cout<<tinyLines[i]->reg;

            cout<<endl;
        }

		cout <<"sys halt"<<endl;
    }

};


class IRNodeList
{
    public:
    vector<IRNode*> codeLines;
	
    int tempVarNum;
	int labelnum;
    string temp;
	string temp2;
	string temp3;
	string oprs,regs;
	std::stringstream buff; 
    IRNodeList()
    {
        tempVarNum = 0;
		labelnum=1;
    }
    
    void addNode(IRNode* node)
    {
        codeLines.push_back(node);
    }

	string convert(string a)
		{
			string t;
			string tc;
			tc=a.substr(0,1);
			if (tc =="$")
				return ("r" + a.substr(2,(a.size()-2)));
			else 
				return a;
			
		}
	//to check if memory val or not
	bool checkmem(string a)
		{
			if (a.substr(0,1) =="$")
				{return false;}
			else {return true;}
		}	
 
    void print()
    {
		cout<<";IR code"<<endl;
		
        for (int i = 0; i < codeLines.size(); i++)
        {
            cout<<";";
            cout<<codeLines[i]->opcode<<" ";
            if (codeLines[i]->isIntLiteral)
            {
                cout<<codeLines[i]->intLiteral<<" ";
            }
            else if (codeLines[i]->isFloatLiteral)
                cout<<codeLines[i]->floatLiteral<<" ";
            else
            {
                if (codeLines[i]->firstOperand != NULL)
                    cout<<codeLines[i]->firstOperand->name<<" ";
                if (codeLines[i]->secondOperand != NULL)
                    cout<<codeLines[i]->secondOperand->name<<" ";
            }
            cout<<codeLines[i]->result->name;
            cout<<endl;
        }
		cout<<";tiny code"<<endl;
    }

	void gencode( tinyNodeList *tnl)
	{
		
		for (int i=0;i<codeLines.size(); i++)
		{
			
			temp2= codeLines[i]->opcode;
			if( (temp2 == "STOREI")|| (temp2 == "STOREF")) 
            {
                if( temp2 == "STOREI") {temp="move";} 
                if( temp2 == "STOREF") {temp="move";} 
                
				
              if ((codeLines[i]->isIntLiteral) || (codeLines[i]->isFloatLiteral))
			    {
               	 if (codeLines[i]->isIntLiteral)
               	 {
               	     buff<<codeLines[i]->intLiteral;
               	     oprs=buff.str();
               	     buff.str("");
               	 }
               	 else if (codeLines[i]->isFloatLiteral)
               	  {
					  buff<<codeLines[i]->floatLiteral;
               	      oprs=buff.str();
               	      buff.str("");
                  }
               	 else
               	 {
               	     if (codeLines[i]->firstOperand != NULL)
               	         {oprs=convert(codeLines[i]->firstOperand->name);}
               	     if (codeLines[i]->secondOperand != NULL)
               	         {oprs=convert(codeLines[i]->secondOperand->name);} 
               	 }
				
                	regs=convert(codeLines[i]->result->name);        
                	tnl->addNode(new tinyNode(temp,oprs,regs));	
				}
			else {
					
					 if (codeLines[i]->firstOperand != NULL)
               	         {oprs=convert(codeLines[i]->firstOperand->name);}
			
               	     else if (codeLines[i]->secondOperand != NULL)
               	         {oprs=convert(codeLines[i]->secondOperand->name);} 

                	tnl->addNode(new tinyNode("move",oprs,"r0"));	
                	regs=convert(codeLines[i]->result->name);        
                	tnl->addNode(new tinyNode(temp,"r0",regs));	

				  }
       
            }

			if( (temp2 == "EQ") || (temp2 == "NE") || (temp2 == "GE") || (temp2 == "LE") || (temp2 == "GT") || (temp2 == "LT") ) 
            {
                if( temp2 == "EQ") {temp="jeq";} 
                if( temp2 == "NE") {temp="jne";} 
                if( temp2 == "GE") {temp="jge";} 
                if( temp2 == "LE") {temp="jle";} 
                if( temp2 == "GT") {temp="jgt";} 
                if( temp2 == "LT") {temp="jlt";}
				if ( codeLines[i]->firstOperand->type == Int ) {temp3="cmpi";}
				if ( codeLines[i]->firstOperand->type == Float ) {temp3="cmpr";}
				oprs=convert(codeLines[i]->firstOperand->name);	
                regs=convert(codeLines[i]->secondOperand->name);
				if (checkmem(regs))
				  {
						tnl->addNode(new tinyNode("move",regs,"r0"));
						tnl->addNode(new tinyNode(temp3,oprs,"r0"));
				   }
				else
	             {   tnl->addNode(new tinyNode(temp3,oprs,regs));}		

                regs=convert(codeLines[i]->result->name);
                tnl->addNode(new tinyNode(temp,regs));	 
                    
            }

	if( (temp2 == "LABEL") || (temp2 == "JUMP")|| (temp2 == "READI") || (temp2 == "WRITEI") || (temp2 == "READF") || (temp2 == "WRITEF") || (temp2 == "WRITES") || (temp2 == "READS") ) 
            {
                if( temp2 == "JUMP") {temp="jmp";} 
                if( temp2 == "LABEL") {temp="label";} 
                if( temp2 == "READI") {temp="sys readi";} 
                if( temp2 == "WRITEI") {temp="sys writei";} 
                if( temp2 == "READF") {temp="sys readr";} 
                if( temp2 == "WRITEF") {temp="sys writer";} 
                if( temp2 == "READS") {temp="sys reads";} 
                if( temp2 == "WRITES") {temp="sys writes";} 
                regs=convert(codeLines[i]->result->name);        
                tnl->addNode(new tinyNode(temp,regs));	
                //tnl->print();      
            }


			if( temp2 == "ADDI" || temp2 =="MULTI" ||temp2=="DIVI" || temp2=="SUBI"|| temp2 == "ADDF" || temp2 =="MULTF" ||temp2=="DIVF" ||temp2=="SUBF" ) 
            {
                if (temp2=="ADDI") {temp="addi";}
                if (temp2=="MULTI"){temp="muli";}
                if (temp2=="DIVI"){temp="divi";}
                if (temp2=="SUBI"){temp="subi";}
                if (temp2=="ADDF") {temp="addr";}
                if (temp2=="MULTF"){temp="mulr";}
                if (temp2=="DIVF"){temp="divr";}
                if (temp2=="SUBF"){temp="subr";}

                oprs=convert(codeLines[i]->firstOperand->name);	
                regs="r0";
                tnl->addNode(new tinyNode("move",oprs,regs));		

                if (codeLines[i]->secondOperand != NULL)
                    {oprs=convert(codeLines[i]->secondOperand->name);}
                 
                regs="r0";
                tnl->addNode(new tinyNode(temp,oprs,regs));	
 
                oprs="r0";
                regs=convert(codeLines[i]->result->name);

                tnl->addNode(new tinyNode("move",oprs,regs));
            } 
        }
    } 
};


#endif //IRNODE_H
